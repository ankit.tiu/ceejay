// const User = require("../models/users.model.js");
const Round = require("../models/Round.js");

// Create and Save a new Users
exports.create = async (req, res) => {

  // res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  if(req.body.roundId == undefined) res.send(await Round.create(req.body));
  else res.send(await Round.update(req.body, {where : { roundId : req.body.roundId}}));

  // Create a Users
  // const user = new User({
  //   user_id : req.body.user_id,
  //   user_name : req.body.user_name,
  //   user_mobile : req.body.user_mobile,
  //   user_email : req.body.user_email,
  //   user_city : req.body.user_city,
  //   user_state : req.body.user_state,
  //   user_username : req.body.user_username,
  //   user_userpassword : req.body.user_userpassword,
  //   user_google_id : req.body.user_google_id,
  //   user_fb_id : req.body.user_fb_id,
  //   user_status : req.body.user_status,
  //   user_creatated_at : req.body.user_creatated_at,
  //   user_updated_at : req.body.user_updated_at,
  //   user_fcm_token : req.body.user_fcm_token,
  // });

  // Save User in the database
  // User.create(user, (err, data) => {
  //   if (err)
  //     res.status(500).send({
  //       status_code : 201,
  //       message:
  //         err.message || "Some error occurred while creating the User."
  //     });
  //   else res.send(data);
  // });
};

// Retrieve all User from the database.
exports.findAll = async (req, res) => {
  res.send(await Round.findAll({ order: ["roundName"]}));
  // Round.getAll((err, data) => {
  //   if (err)
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while retrieving User."
  //     });
  //   else res.send(data);
  // });
};

// Find a single User with a user_id
exports.findOne = (req, res) => {
  Round.findById(req.params.round_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found user with id ${req.params.user_id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving user with id " + req.params.user_id
        });
      }
    } else res.send(data);
  });
};

// Update a User identified by the user_id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  Users.updateById(
    req.params.user_id,
    new Customer(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found User with id ${req.params.user_id}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating User with id " + req.params.user_id
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a User with the specified user_id in the request
exports.delete = (req, res) => {
  Users.remove(req.params.user_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.user_id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Customer with id " + req.params.user_id
        });
      }
    } else res.send({ message: `Customer was deleted successfully!` });
  });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  Users.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else res.send({ message: `All Customers were deleted successfully!` });
  });
};
