// const Party = require("../models/partys.model.js");
const { transforms } = require("json2csv");
const Csv = require("../models/csv.js");
const Lot = require("../models/lot.js");
const Party = require("../models/Party.js");
const Party_group = require("../models/Party_group.js");
const Party_lot = require("../models/Party_lot.js");
const sequelize = require("../util/database");
const { Op, QueryTypes } = require("sequelize");
exports.set_round_party = async (req, res) => {
    if (req.body.party_list != undefined) {
        res.send({ "action": await Party.set_round_party(req.body) });
    } else {
        res.send(req.body);
    }
}

exports.loadpartywise = async (req, res) => {
    if (req.query.file_type != undefined && req.query.file_type == "csv") {
        var rows = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.query.financial_year_id,
                stateStateId: req.query.state_id,
                roundRoundId: req.query.round_id
            },
            include: [{
                model: Party_lot,
                attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority", "party_lot_status", "party_lot_rank"],
                where: {
                    financialYearFinancialYearId: req.query.financial_year_id,
                    stateStateId: req.query.state_id,
                    roundRoundId: req.query.round_id
                },
                include: [{
                    model: Lot,
                    where: {
                        financialYearFinancialYearId: req.query.financial_year_id,
                        stateStateId: req.query.state_id,
                        // roundRoundId: req.body.round_id
                    },
                }],
            }],
            order: [
                ["party_head_code", "ASC"],
                [Party_lot, 'party_lot_rank', 'ASC'],
                [Party_lot, 'party_lot_priority', 'ASC'],
                [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
                // [ Lot, "lotId"]
                // [ Party_lot, 'party_lot_priority', 'DESC'],

            ],
        });
        $output = [];
        //rows.forEach(x => {
        for (let i = 0; i < rows.length; i++) {
            for (let index = 0; index < rows[i].party_lot_data.length; index++) {
                $value = {};
                $value.head_code = rows[i].party_head_code;
                $value.party_lot_priority = rows[i].party_lot_data[index].party_lot_priority;
                $value.lotNo = rows[i].party_lot_data[index].Lot.lotNo;
                $value.lotLRATE = rows[i].party_lot_data[index].party_lot_pur_rate;
                $value.round_id = req.session.master.round_id;
                $value.party_name = rows[i].party_name;
                $value.party_bidding_capacity = rows[i].party_bidding_capacity;
                $value.party_emd = rows[i].party_emd;
                $value.lotSTANB = rows[i].party_lot_data[index].Lot.lotSTANB;
                $value.party_lot_qty_per_rate = rows[i].party_lot_data[index].party_lot_qty_per_rate;
                $output.push($value)
            }
        }

        // });
        // console.log($output);

        var title = '"Head Code","Pr.No","Lot No","RATE","Round","Party","Bidding Capacity", "Emd","lotSTANB","party_lot_qty_per_rate"\n';
        //Csv.get_csv(['party_head_code', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_lot_data.length'], rows, title, res);
        Csv.get_csv(['head_code', 'party_lot_priority', 'lotNo', 'lotLRATE', 'round_id', 'party_name', 'party_bidding_capacity', 'party_emd', 'lotSTANB', 'party_lot_qty_per_rate'], $output, title, res);
    } else {
        var rows = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.body.financial_year_id,
                stateStateId: req.body.state_id,
                roundRoundId: req.body.round_id
            },
            attributes: ['party_id', 'party_name', 'party_display_name', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_head_code', 'party_status', 'party_balance'],
            include: [{
                model: Party_lot,
                attributes: ['partyPartyId', 'party_lot_id', 'party_lot_priority', 'party_lot_pur_rate', 'party_lot_qty_per_rate', 'party_lot_rank', 'party_lot_result_flag', 'party_lot_result_status', 'party_lot_status'],
                where: {
                    financialYearFinancialYearId: req.body.financial_year_id,
                    stateStateId: req.body.state_id,
                    roundRoundId: req.body.round_id
                },
                include: [{
                    model: Lot,
                    attributes: ['lotId','lotNo', 'lotDISTU', 'lotName', 'lotSTANB', 'lotLRATE'],
                    where: {
                        financialYearFinancialYearId: req.body.financial_year_id,
                        stateStateId: req.body.state_id,
                        // roundRoundId: req.body.round_id
                    },
                }],
            }],
            order: [
                ["party_head_code", "ASC"],
                [Party_lot, 'party_lot_rank', 'ASC'],
                [Party_lot, 'party_lot_priority', 'ASC'],
                [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
            ],
        });

        // rows.forEach((x,i) => {
        //   x.party_lot_data.forEach((y, ii) => {
        //     if( y.party_lot_qty_per_rate <= x.party_bidding_capacity){
        //       //rows[i].party_lot_data[ii].party_lot_status = x.party_lot_rank >= 2 ? "Luckey Rate" :  "ALLOTED";
        //       y.party_lot_status = x.party_lot_rank >= 2 ? "Luckey Rate" :  "ALLOTED";
        //       x.party_bidding_capacity = x.party_bidding_capacity - y.party_lot_qty_per_rate;
        //       // console.log(x.toJSON());
        //     }else{
        //       // y.party_lot_status = "CAP. OVER";
        //     }
        //   });
        // });



        res.send(rows);
    }

    // var output = await Party_lot.loadpartywise(req.body);
    // var flag = true;
    // for (let index = 0; index < output.length; index++) {
    //   if(flag){
    //     req.body.party_id = output[index].party_id;
    //     output[index].list = await Party_lot.loadpartywise(req.body);
    //     console.log(output);
    //     flag = false;
    //   }
    // }
    // output.forEach(async (element,index) => {
    //   req.body.party_id = element.party_id;
    //   // output[index].list = await Party_lot.loadpartywise(req.body);
    //   console.log(await Party_lot.loadpartywise(req.body));
    // });
    // res.send(output);
}

exports.loadpartywise_reslut = async (req, res) => {
    if (req.query.file_type != undefined && req.query.file_type == "csv") {
        var rows = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.query.financial_year_id,
                stateStateId: req.query.state_id,
                roundRoundId: req.query.round_id,
            },
            include: [{
                model: Party_lot,
                attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority", "party_lot_status", "party_lot_rank"],
                where: {
                    party_lot_result_status: true,
                    financialYearFinancialYearId: req.query.financial_year_id,
                    stateStateId: req.query.state_id,
                    roundRoundId: req.query.round_id,
                },
                include: [{
                    model: Lot,
                    where: {
                        financialYearFinancialYearId: req.query.financial_year_id,
                        stateStateId: req.query.state_id,
                        // roundRoundId: req.body.round_id
                    },
                }],
            }],
            order: [
                ["party_head_code", "ASC"],
                [Party_lot, 'party_lot_rank', 'ASC'],
                [Party_lot, 'party_lot_priority', 'ASC'],
                [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
                // [ Lot, "lotId"]
                // [ Party_lot, 'party_lot_priority', 'DESC'],

            ],
        });
        $output = [];
        //rows.forEach(x => {
        for (let i = 0; i < rows.length; i++) {
            for (let index = 0; index < rows[i].party_lot_data.length; index++) {
                $value = {};
                $value.head_code = rows[i].party_head_code;
                $value.party_lot_priority = rows[i].party_lot_data[index].party_lot_priority;
                $value.lotNo = rows[i].party_lot_data[index].Lot.lotNo;
                $value.lotLRATE = rows[i].party_lot_data[index].party_lot_pur_rate;
                $value.round_id = req.session.master.round_id;
                $value.party_name = rows[i].party_name;
                $value.party_bidding_capacity = rows[i].party_bidding_capacity;
                $value.party_emd = rows[i].party_emd;
                $value.lotSTANB = rows[i].party_lot_data[index].Lot.lotSTANB;
                $value.party_lot_qty_per_rate = rows[i].party_lot_data[index].party_lot_qty_per_rate;
                $output.push($value)
            }
        }

        // });
        // console.log($output);

        var title = '"Head Code","Pr.No","Lot No","RATE","Round","Party","Bidding Capacity", "Emd","lotSTANB","party_lot_qty_per_rate"\n';
        //Csv.get_csv(['party_head_code', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_lot_data.length'], rows, title, res);
        Csv.get_csv(['head_code', 'party_lot_priority', 'lotNo', 'lotLRATE', 'round_id', 'party_name', 'party_bidding_capacity', 'party_emd', 'lotSTANB', 'party_lot_qty_per_rate'], $output, title, res);
    } else {
        var rows = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.body.financial_year_id,
                stateStateId: req.body.state_id,
                roundRoundId: req.body.round_id
            },
            attributes: ['party_id', 'party_name', 'party_display_name', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_head_code', 'party_status', 'party_balance'],
            include: [{
                model: Party_lot,
                attributes: ['partyPartyId', 'party_lot_id', 'party_lot_priority', 'party_lot_pur_rate', 'party_lot_qty_per_rate', 'party_lot_rank', 'party_lot_result_flag', 'party_lot_result_status', 'party_lot_status'],
                where: {
                    financialYearFinancialYearId: req.body.financial_year_id,
                    stateStateId: req.body.state_id,
                    roundRoundId: req.body.round_id,
                    party_lot_result_status: true,
                },
                include: [{
                    model: Lot,
                    attributes: ['lotNo', 'lotDISTU', 'lotName', 'lotSTANB', 'lotLRATE'],
                    where: {
                        financialYearFinancialYearId: req.body.financial_year_id,
                        stateStateId: req.body.state_id,
                        // roundRoundId: req.body.round_id
                    },
                }],
            }],
            order: [
                ["party_head_code", "ASC"],
                [Party_lot, 'party_lot_rank', 'ASC'],
                [Party_lot, 'party_lot_priority', 'ASC'],
                [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
            ],
        });

        // rows.forEach((x,i) => {
        //   x.party_lot_data.forEach((y, ii) => {
        //     if( y.party_lot_qty_per_rate <= x.party_bidding_capacity){
        //       //rows[i].party_lot_data[ii].party_lot_status = x.party_lot_rank >= 2 ? "Luckey Rate" :  "ALLOTED";
        //       y.party_lot_status = x.party_lot_rank >= 2 ? "Luckey Rate" :  "ALLOTED";
        //       x.party_bidding_capacity = x.party_bidding_capacity - y.party_lot_qty_per_rate;
        //       // console.log(x.toJSON());
        //     }else{
        //       // y.party_lot_status = "CAP. OVER";
        //     }
        //   });
        // });



        res.send(rows);
    }

    // var output = await Party_lot.loadpartywise(req.body);
    // var flag = true;
    // for (let index = 0; index < output.length; index++) {
    //   if(flag){
    //     req.body.party_id = output[index].party_id;
    //     output[index].list = await Party_lot.loadpartywise(req.body);
    //     console.log(output);
    //     flag = false;
    //   }
    // }
    // output.forEach(async (element,index) => {
    //   req.body.party_id = element.party_id;
    //   // output[index].list = await Party_lot.loadpartywise(req.body);
    //   console.log(await Party_lot.loadpartywise(req.body));
    // });
    // res.send(output);
}



exports.capacity = async (req, res) => {

    // var list = await Party_lot.findAll({ where : { partyPartyId : 161}});
    // for (const i of list) await i.deleteAll();

    // console.log(await Party_lot.findAll({ where : { partyPartyId : 161}}));



    if (req.query.file_type != undefined && req.query.file_type == "csv") {
        var list = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.query.financial_year_id,
                roundRoundId: req.query.round_id,
                stateStateId: req.query.state_id
            },
            // include: [Lot, Party_lot],
            order: ["party_head_code"]
        });
        var title = '"Name","Emd","Purchase Capacity","Bidding Capacity","Tander Count"\n';
        Csv.get_csv(['party_name', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_lot_data.length'], list, title, res);
    } else {
        res.send(await Party.findAll({
            where: {
                financialYearFinancialYearId: req.body.financial_year_id,
                roundRoundId: req.body.round_id,
                stateStateId: req.body.state_id
            },
            attributes: ['party_id', "party_head_code", 'party_display_name', 'party_name', 'party_purchase_capacity'],
            include: [Party_lot], //Lot, 
            order: ["party_head_code"],
            //  include: [Party_group]
        }));
    }
}

exports.allocateRank = async (req, res) => {
    // roundRoundId: req.body.round_id,
    var row = await Lot.findAll({
        where: { stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
        include: [{
            model: Party_lot,
            where: {roundRoundId: req.body.round_id},
            attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority", 'party_lot_rank', 'party_lot_id'],
            include: [Party],
        }],
        attributes: ["lotDISTU", "lotName", "lotNo", "lotSTANB", "lotNameU", "lotACTB"],
        order: [
            [Party_lot, 'party_lot_pur_rate', 'DESC'],
            [Party_lot, 'party_lot_qty_per_rate', 'DESC'],
            [Party_lot, 'party_lot_priority', 'ASC'],
            // [ Party, "lotName", "ASC"]
        ],
    });
    var d = {};
    row.forEach(element => {
        element.party_lot_data.forEach(async (x, i) => {
            x.party_lot_rank = i + 1;
            Party_lot.update({ party_lot_rank: i + 1 }, { where: { party_lot_id: x.party_lot_id } });
        });
    });
    res.send(row);

    // var rows = await Party.findAll({
    //   where: {
    //     financialYearFinancialYearId : req.body.financial_year_id,
    //     stateStateId : req.body.round_id,
    //     roundRoundId : req.body.state_id
    //   },
    //   include: [{
    //     model : Party_lot,
    //     attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority"],
    //     // include : [Lot],
    //   },Lot],
    //   order: [
    //     [ Party_lot, 'party_lot_pur_rate', 'DESC'],
    //     [ Party_lot, 'party_lot_qty_per_rate', 'DESC'],
    //     [ Party_lot, 'party_lot_priority', 'ASC'],
    //     // [ Party, "lotName", "ASC"]
    //   ],
    // });
    // res.send(rows);
    // res.send(req.body);
}

exports.allocateLot = async (req, res) => {
    // roundRoundId: req.body.round_id,
    rows = await Party.findAll({
        where: {
            financialYearFinancialYearId: req.body.financial_year_id,

            stateStateId: req.body.state_id
        },
        // Lot,
        include: [{
            model: Party_lot,
            where: {roundRoundId: req.body.round_id},
            include: Lot
        }],
        order: [
            ["party_head_code", "ASC"],
            [Party_lot, 'party_lot_rank', 'ASC'],
            [Party_lot, 'party_lot_priority', 'ASC'],
            [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
        ],
    });

    var Losts = await Lot.findAll({
        where: {
            financialYearFinancialYearId: req.body.financial_year_id,
            // roundRoundId: req.body.round_id,
            stateStateId: req.body.state_id
        },
    });

    for (const x of Losts) x.lotFlag = false

    // Losts.forEach(x => x.lotFlag = false);
    //var r = Losts.find(x=> x.lotId ==1);
    // r.lotFlag = true;
    // res.send(Losts);
    // return;

    for (let round_index = 1; round_index <= 100; round_index++) {
        for (const x of rows) {
            for (const y of x.party_lot_data) {
                if (parseInt(y.party_lot_rank) == round_index) {
                    var f = Losts.find(x => x.lotId == y.Lot.lotId);
                    if (f.lotFlag == false) {
                        if (parseInt(x.party_purchase_capacity) >= parseInt(y.party_lot_qty_per_rate)) {
                            y.party_lot_status = parseInt(y.party_lot_rank) >= 2 ? "Luckey Rate" : "ALLOTED";
                            x.party_purchase_capacity = x.party_purchase_capacity - y.party_lot_qty_per_rate;
                            Losts.find(x => x.lotId == y.Lot.lotId).lotFlag = true;
                            y.save();
                        } else {
                            y.party_lot_status = "CAP. OVER"; //"CAP. OVER";
                            y.save();
                        }
                    } else {
                        y.party_lot_status = "";
                        y.save();
                    }
                }
            }
        }

        // rows.forEach((x, i) => {
        //     x.party_lot_data.forEach((y, ii) => {
        //         if (parseInt(y.party_lot_rank) == round_index) {
        //             var f = Losts.find(x => x.lotId == y.Lot.lotId);
        //             console.log(f);

        //             if (f.lotFlag == false) {
        //                 if (parseInt(x.party_purchase_capacity) >= parseInt(y.party_lot_qty_per_rate)) {
        //                     y.party_lot_status = parseInt(y.party_lot_rank) >= 2 ? "Luckey Rate" : "ALLOTED";
        //                     x.party_purchase_capacity = x.party_purchase_capacity - y.party_lot_qty_per_rate;
        //                     Losts.find(x => x.lotId == y.Lot.lotId).lotFlag = true;
        //                     y.save();
        //                 } else {
        //                     y.party_lot_status = "CAP. OVER"; //"CAP. OVER";
        //                     y.save();
        //                 }
        //             } else {
        //                 y.party_lot_status = "";
        //                 y.save();
        //             }
        //         }
        //     });
        // });
    }

    res.send(rows);
    return;

    // for first renk
    rows.forEach((x, i) => {
        x.party_lot_data.forEach((y, ii) => {
            // if (x.party_id == 1 && y.LotLotId == 588) {
            // if (x.party_id == 1) {
            // console.log(parseInt(x.party_bidding_capacity) >= parseInt(y.party_lot_qty_per_rate));
            // console.log(x.party_bidding_capacity +  " >= " + y.party_lot_qty_per_rate);
            if (parseInt(y.party_lot_rank) == 1) {
                if (parseInt(x.party_bidding_capacity) >= parseInt(y.party_lot_qty_per_rate)) {
                    y.party_lot_status = parseInt(y.party_lot_rank) >= 2 ? "Luckey Rate" : "ALLOTED";
                    x.party_bidding_capacity = x.party_bidding_capacity - y.party_lot_qty_per_rate;
                    var r = Losts.find(x => x.lotId == y.Lot.lotId);
                    r.lotFlag = true;
                    y.save();
                } else {
                    y.party_lot_status = "CAP. OVER";
                    y.save();
                }
            }
            //}
        });
    });

    // for second rank
    rows.forEach((x, i) => {
        x.party_lot_data.forEach((y, ii) => {

            // if (x.party_id == 1 && y.LotLotId == 588) {
            //if (x.party_id == 1) {
            if (parseInt(y.party_lot_rank) == 2) {
                var f = Losts.find(x => x.lotId == y.Lot.lotId);
                // console.log(f.toJSON());
                if (f == false) {
                    if (parseInt(x.party_bidding_capacity) >= parseInt(y.party_lot_qty_per_rate)) {
                        y.party_lot_status = parseInt(y.party_lot_rank) >= 2 ? "Luckey Rate" : "ALLOTED";
                        x.party_bidding_capacity = x.party_bidding_capacity - y.party_lot_qty_per_rate;
                        var r = Losts.find(x => x.lotId == y.Lot.lotId);
                        r.lotFlag = true;
                        y.save();
                    } else {
                        console.log("CAP. OVER");
                        y.party_lot_status = "CAP. OVER";
                        y.save();
                    }
                } else {
                    y.party_lot_status = "---";
                    y.save();
                }
            }
            //  }
        });
    });

    // for thard rank
    rows.forEach((x, i) => {
        x.party_lot_data.forEach((y, ii) => {

            // if (x.party_id == 1 && y.LotLotId == 588) {
            //if (x.party_id == 1) {
            if (parseInt(y.party_lot_rank) == 3) {
                var f = Losts.find(x => x.lotId == y.Lot.lotId);
                // console.log(f.toJSON());
                if (f == false) {
                    if (parseInt(x.party_bidding_capacity) >= parseInt(y.party_lot_qty_per_rate)) {
                        y.party_lot_status = parseInt(y.party_lot_rank) >= 2 ? "Luckey Rate" : "ALLOTED";
                        x.party_bidding_capacity = x.party_bidding_capacity - y.party_lot_qty_per_rate;
                        var r = Losts.find(x => x.lotId == y.Lot.lotId);
                        r.lotFlag = true;
                        y.save();
                    } else {
                        console.log("CAP. OVER");
                        y.party_lot_status = "CAP. OVER";
                        y.save();
                    }
                } else {
                    y.party_lot_status = "---";
                    y.save();
                }
            }
            //  }
        });
    });

    //res.send(Losts.filter(x => x.lotFlag == false));
    //res.send(rows);
}

exports.removeallocateLot = async (req, res) => {
    rows = await Party.findAll({
        where: {
            financialYearFinancialYearId: req.body.financial_year_id,
            roundRoundId: req.body.round_id,
            stateStateId: req.body.state_id
        },
        // Lot,
        include: [{
            model: Party_lot,
            include: Lot
        }],
        order: [
            ["party_head_code", "ASC"],
            [Party_lot, 'party_lot_rank', 'ASC'],
            [Party_lot, 'party_lot_priority', 'ASC'],
            [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
        ],
    });


    rows.forEach((x, i) => {
        x.party_lot_data.forEach((y, ii) => {
            y.party_lot_status = null;
            y.save();
        });
    });


    res.send(true);
}

exports.update_party_lot_data = async (req, res) => {
    await Party_lot.update(req.body, { where: { party_lot_id: req.body.party_lot_id,  }});
    if( req.body.party_lot_result_status) await Lot.update({ roundRoundId : req.session.master.round_id}, { where: { lotId : req.body.Lot.lotId}});
    else await Lot.update({ roundRoundId : null}, { where: { lotId : req.body.Lot.lotId}});
    res.send(true);
}

// Retrieve all Party from the database.
exports.findAll = async (req, res) => {
    res.send(await Party.findAll({
        where: {
            financialYearFinancialYearId: req.body.financial_year_id,
            stateStateId: req.body.state_id,
            roundRoundId: req.body.round_id
        },
        order: [
            ["party_head_code", "ASC"],
            // [Party_lot, 'party_lot_rank', 'ASC'],
            // [Party_lot, 'party_lot_priority', 'ASC'],
            // [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
            // [ Lot, "lotId"]
            // [ Party_lot, 'party_lot_priority', 'DESC'],

        ],
    }));
    // Party.getAll((err, data) => {
    //   if (err)
    //     res.status(500).send({
    //       message:
    //         err.message || "Some error occurred while retrieving Party."
    //     });
    //   else res.send(data);
    // });
};

exports.load_focast = async (req, res) => {
    // roundRoundId: req.query.round_id
    //roundRoundId: req.query.round_id,
    if (req.query.file_type != undefined && req.query.file_type == "csv") {
        var rows = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.query.financial_year_id,
                stateStateId: req.query.state_id,
               
            },
            attributes: ['party_id', 'party_name', 'party_display_name', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_head_code', 'party_status', 'party_balance'],

            include: [{
                model: Party_lot,
                attributes: ['partyPartyId', 'party_lot_id', 'party_lot_priority', 'party_lot_pur_rate', 'party_lot_qty_per_rate', 'party_lot_rank', 'party_lot_result_flag', 'party_lot_result_status', 'party_lot_status'],
                where: {
                    financialYearFinancialYearId: req.query.financial_year_id,
                    stateStateId: req.query.state_id,
                    
                    party_lot_rank: { [Op.gte]: 0 },
                    [Op.or]: [{ party_lot_status: 'ALLOTED' }, { party_lot_status: 'Luckey Rate' }],
                },
                // include: [{
                //     model: Lot,
                //     where: {
                //         financialYearFinancialYearId: req.query.financial_year_id,
                //         stateStateId: req.query.state_id,
                //         // roundRoundId: req.body.round_id
                //     },
                // }],
            }],
            order: [
                ["party_head_code", "ASC"],
                [Party_lot, 'party_lot_rank', 'ASC'],
                [Party_lot, 'party_lot_priority', 'ASC'],
                [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
                // [ Lot, "lotId"]
                // [ Party_lot, 'party_lot_priority', 'DESC'],

            ],
        });
        var title = '"Head Code","Pr.No","Lot No","RATE","Round"\n';
        //Csv.get_csv(['party_head_code', 'party_emd', 'party_purchase_capacity', 'party_bidding_capacity', 'party_lot_data.length'], rows, title, res);
        Csv.get_csv(['party_head_code'], rows, title, res);
    } else {
        var rows = await Party.findAll({
            where: {
                financialYearFinancialYearId: req.body.financial_year_id,
                stateStateId: req.body.state_id,
                roundRoundId: req.body.round_id
            },
            include: [{
                model: Party_lot,
                where: {
                    financialYearFinancialYearId: req.body.financial_year_id,
                    stateStateId: req.body.state_id,
                    
                    party_lot_pur_rate: { [Op.not]: 0 },
                    [Op.or]: [{ party_lot_status: 'ALLOTED' }, { party_lot_status: 'Luckey Rate' }],
                    // party_lot_status: 'ALLOTED' || 'Luckey Rate'
                },
                include: [{
                    model: Lot,
                    where: {
                        financialYearFinancialYearId: req.body.financial_year_id,
                        stateStateId: req.body.state_id,
                        
                    },
                }],
            }],
            order: [
                ["party_head_code", "ASC"],
                [Party_lot, 'party_lot_rank', 'ASC'],
                [Party_lot, 'party_lot_priority', 'ASC'],
                [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
                // [ Lot, "lotId"]
                // [ Party_lot, 'party_lot_priority', 'DESC'],

            ],
        });

        // rows.forEach((x,i) => {
        //   x.party_lot_data.forEach((y, ii) => {
        //     if( y.party_lot_qty_per_rate <= x.party_bidding_capacity){
        //       //rows[i].party_lot_data[ii].party_lot_status = x.party_lot_rank >= 2 ? "Luckey Rate" :  "ALLOTED";
        //       y.party_lot_status = x.party_lot_rank >= 2 ? "Luckey Rate" :  "ALLOTED";
        //       x.party_bidding_capacity = x.party_bidding_capacity - y.party_lot_qty_per_rate;
        //       // console.log(x.toJSON());
        //     }else{
        //       // y.party_lot_status = "CAP. OVER";
        //     }
        //   });
        // });



        res.send(rows);
    };
}

exports.set_party_lot = async (req, res) => {

    if (!(req.body.party == undefined || req.body.party.party_id == undefined)) {
        await Party.update({
            "party_emd": req.body.party.party_emd,
            "party_purchase_capacity": req.body.party.party_purchase_capacity,
            "party_bidding_capacity": req.body.party.party_bidding_capacity
        }, {
            where: {
                party_id: req.body.party.party_id
            }
        })
        await Party_lot.save_array(req.body.search, req.body.party, req.body.party_lot);
        res.send(req.body);
    } else res.send(false);
}

// Retrieve all Party by round from the database.
exports.get_party_by_round = async (req, res) => {
    res.send(await Party.findAll({
        where: {
            financialYearFinancialYearId: req.body.financial_year_id,
            stateStateId: req.body.state_id,
            roundRoundId: req.body.round_id
        }
    }));
};

exports.set = async (req, res) => {
    // console.log(req.body);
    Party.update({
        party_display_name: req.body.party_display_name
    }, {
        where: {
            party_id: req.body.party_id
        }
    })

    res.send(true);
};

exports.get_party_wise_summery = async (req, res) => {
    var list = '';
    list = await Party_lot.findAll({
        attributes: ['partyPartyId', 
            // sequelize.col('party.party_head_code'),
            [sequelize.fn('sum', sequelize.col('party_lot_qty_per_rate')), 'party_lot_qty_per_rate',],
            [sequelize.fn('sum', sequelize.col('Lot.lotSTANB')), 'bags'],
            [sequelize.fn('count', sequelize.col('party_lot_id')), 'lott_no']
        ],
        where: { roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id, party_lot_result_status: true },
        group: ['partyPartyId'],
        include: [{
            model : Party
        }, Lot],
        order:[sequelize.col('party.party_head_code')],
    })
    // const list = await sequelize.query(`SELECT *,(SELECT SUM(lots.lotSTANB * lots.lotLRATE) FROM party_lot_data LEFT JOIN lots on lots.lotId = party_lot_data.LotLotId
    // WHERE party_lot_data.partyPartyId = parties.party_id and party_lot_data.party_lot_result_status = 1) as totamnt, (SELECT COUNT(*) FROM party_lot_data WHERE party_lot_data.partyPartyId = parties.party_id and party_lot_data.party_lot_result_status = 1) as totLot, (SELECT SUM(lots.lotSTANB) FROM party_lot_data LEFT JOIN lots on lots.lotId = party_lot_data.LotLotId
    // WHERE party_lot_data.partyPartyId = parties.party_id and party_lot_data.party_lot_result_status = 1) as totbag FROM parties WHERE financialYearFinancialYearId = ${req.body.financial_year_id} AND stateStateId = ${req.body.state_id} AND roundRoundId =  ${req.body.round_id} and (SELECT COUNT(*) FROM party_lot_data WHERE party_lot_data.partyPartyId = parties.party_id and party_lot_data.party_lot_result_status = 1) > 0`, { type: QueryTypes.SELECT });
    res.send(list);
};

exports.delete_all_loats = async (req, res) => {
    // console.log(req.body);
    // res.send(true);
    // return;
    await sequelize.query("DELETE FROM party_lot_data WHERE partyPartyId = " + req.body.x.party_id, { type: QueryTypes.DELETE });
    res.send(true);
};