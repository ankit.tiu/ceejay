const { Op } = require('sequelize');
// const User = require("../models/users.model.js");
const Financial_year = require("../models/Financial_year.js");
var fs = require('fs');
var parse = require('csv-parse');
const Lot = require("../models/lot.js");
const Party_lot = require('../models/Party_lot.js');

// Create and Save a new Users
exports.create = async (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  if (!req.body.financial_year_id) {
    var year = await Financial_year.create(req.body);
    await this.save_lots("uploaded/LOT MP WITH LAST RESULT.csv", year.financial_year_id, 1);
    await this.save_lots("uploaded/LOT-CG WITH LAST RESULT.csv", year.financial_year_id, 2);
    res.send(year);
  }
  else res.send(await Financial_year.update(req.body, { where: { financial_year_id: req.body.financial_year_id } }));

  // Create a Users
  // const Financial_year = new User({
  //   // user_id : req.body.user_id,
  //   // user_name : req.body.user_name,
  //   // user_mobile : req.body.user_mobile,
  //   // user_email : req.body.user_email,
  //   // user_city : req.body.user_city,
  //   // user_state : req.body.user_state,
  //   // user_username : req.body.user_username,
  //   // user_userpassword : req.body.user_userpassword,
  //   // user_google_id : req.body.user_google_id,
  //   // user_fb_id : req.body.user_fb_id,
  //   // user_status : req.body.user_status,
  //   // user_creatated_at : req.body.user_creatated_at,
  //   // user_updated_at : req.body.user_updated_at,
  //   // user_fcm_token : req.body.user_fcm_token,
  // });

  // // Save User in the database
  // Financial_year.create(user, (err, data) => {
  //   if (err)
  //     res.status(500).send({
  //       status_code : 201,
  //       message:
  //         err.message || "Some error occurred while creating the User."
  //     });
  //   else res.send(data);
  // });
};

// Retrieve all User from the database.
exports.findAll = async (req, res) => {
  res.send(await Financial_year.getAll());
  // Financial_year.getAll((err, data) => {
  //   if (err)
  //     res.status(500).send({
  //       message:
  //         err.message || "Some error occurred while retrieving User."
  //     });
  //   else res.send(data);
  // });
};

// Find a single User with a user_id
exports.findOne = (req, res) => {
  Financial_year.findById(req.params.financial_year_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found user with id ${req.params.user_id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving user with id " + req.params.user_id
        });
      }
    } else res.send(data);
  });
};

// Update a User identified by the user_id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  Financial_year.updateById(
    req.params.user_id,
    new Customer(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found User with id ${req.params.user_id}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating User with id " + req.params.user_id
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a User with the specified user_id in the request
exports.delete = (req, res) => {
  Financial_year.remove(req.params.user_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.user_id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Customer with id " + req.params.user_id
        });
      }
    } else res.send({ message: `Customer was deleted successfully!` });
  });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  Financial_year.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else res.send({ message: `All Customers were deleted successfully!` });
  });
};


exports.save_lots = async (file, financial_year_id, state_id) => {
  var csvData = [];
  await fs.createReadStream(file)
    .pipe(parse({ delimiter: ',' }))
    .on('data', x => csvData.push(x))
    .on('end', async () => {
      //do something with csvData
      for (const x of csvData) {
        var lot = {};
        lot.lotNo = x[1];
        lot.lotNameU = x[2];
        lot.lotName = x[3];
        lot.lotPlace = x[4];
        lot.lotGodawn = x[5];
        lot.lotACTB = x[6];
        lot.lotSTANB = x[7];
        lot.lotDISTU = x[8];
        lot.lotOption = x[11];
        lot.lotUpset = x[12];
        lot.lotLRATE = x[13];
        lot.lotWT = x[14];
        lot.lotROUNDPASS = x[15];
        lot.financialYearFinancialYearId = financial_year_id;
        lot.stateStateId = state_id;
        await Lot.save(lot);
        // console.log(lot);
      }

      var lots = await Lot.findAll({
        where: { stateStateId: state_id, financialYearFinancialYearId: financial_year_id },
        order: [
          // [ Lot, "lotNo"],
          // [ Lot, 'lotDISTU', 'ASC'],
          // [ Party_lot, 'party_lot_qty_per_rate', 'DESC'],
          // [ Party_lot, 'party_lot_priority', 'ASC'],
          // [ Party, "lotName", "ASC"]
        ],
      });
    
      for (let index = 0; index < lots.length; index++) {
        var num = "";
        var str = "";
        num += isNumber(lots[index].lotNo.charAt(0)) ? lots[index].lotNo.charAt(0) : "";
        num += isNumber(lots[index].lotNo.charAt(1)) ? lots[index].lotNo.charAt(1) : "";
        num += isNumber(lots[index].lotNo.charAt(2)) ? lots[index].lotNo.charAt(2) : "";
        num += isNumber(lots[index].lotNo.charAt(3)) ? lots[index].lotNo.charAt(3) : "";
        num += isNumber(lots[index].lotNo.charAt(4)) ? lots[index].lotNo.charAt(4) : "";
        num = num.trim();
    
        str += isNumber(lots[index].lotNo.charAt(0)) ? "" : lots[index].lotNo.charAt(0);
        str += isNumber(lots[index].lotNo.charAt(1)) ? "" : lots[index].lotNo.charAt(1);
        str += isNumber(lots[index].lotNo.charAt(2)) ? "" : lots[index].lotNo.charAt(2);
        str += isNumber(lots[index].lotNo.charAt(3)) ? "" : lots[index].lotNo.charAt(3);
        str += isNumber(lots[index].lotNo.charAt(4)) ? "" : lots[index].lotNo.charAt(4);
        str.trim();
    
        lots[index].lotInd = parseInt(num);
        lots[index].lotTxt = str;
        await Lot.update({ lotInd: parseInt(num), lotTxt: str }, { where: { lotId: lots[index].lotId } });
        // console.log(lots[index]);
        // await Lot.update(lots[index], { where: { lotId: lots[index].lotId } });

        function isNumber(n) {
          return !isNaN(parseFloat(n)) && !isNaN(n - 0);
        }
      }
    });
}

exports.refresh = async (req, res) => {
  var lots = await Lot.findAll({
    where:{
      stateStateId: req.body.master.state_id,
      financialYearFinancialYearId: parseInt(req.body.x.financial_year_id-1),
      roundRoundId: {
        [Op.ne]: null
      }
    },
    include: [
      {
        model: Party_lot,
        where: {
          party_lot_result_status: {
            [Op.eq]: 1
          }
          // party_lot_status:{
          //   [Op.and] : {
          //     [Op.ne]: 'ALLOTED',
          //     [Op.ne]: 'Luckey Rate',
          //     [Op.ne]: 'CAP. OVER'
          //   }
          // }
      }
    }],
  });

  console.log(lots[0]);

  //console.log(lots[0].party_lot_data[0].party_lot_pur_rate);

  for (const x of lots) {
    // console.log(x.lotNo + " " + x.party_lot_data[0].party_lot_pur_rate);
    await Lot.update(
      {party_lot_pur_rate : x.party_lot_data[0].party_lot_pur_rate},
      {where: {lotNo: x.lotNo}}
    );
  }
  res.send(true);
};