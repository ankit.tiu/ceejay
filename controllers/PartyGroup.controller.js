const Lot = require("../models/lot");
const Party = require("../models/Party");
const Party_group = require("../models/Party_group");
const Party_lot = require("../models/Party_lot");
const sequelize = require("../util/database");
const { Op } = require("sequelize");

exports.add_partyGroup = async(req, res) => {
    console.log(req.body);
    res.send(await Party_group.create(req.body));
}

exports.get_partyGroup = async(req, res) => {
    res.send(await Party_group.findAll({
        where: {
            financialYearFinancialYearId: req.body.financial_year_id,
            stateStateId: req.body.state_id,
            roundRoundId: req.body.round_id
        },
        include: [{
            model: Party,
        }],
    }));
}

exports.PartyGroupNew = async(req, res) => {
    var lite = await Party_group.findAll({
        where: {
            financialYearFinancialYearId: req.session.master.financial_year_id,
            stateStateId: req.session.master.state_id,
            roundRoundId: req.session.master.round_id
        }
    });

    var lite1 = await Party_group.findAll({
        where: {
            financialYearFinancialYearId: req.session.master.financial_year_id,
            stateStateId: req.session.master.state_id,
            roundRoundId: req.body.new.round_id
        }
    });

    if (lite1.length == 0) {
        lite.forEach(async x => {
            var pg = {};
            pg.financialYearFinancialYearId = x.financialYearFinancialYearId;
            pg.stateStateId = x.stateStateId;
            pg.party_group_name = x.party_group_name;
            pg.roundRoundId = req.body.new.round_id;
            await Party_group.create(pg);
        });
    }

    // console.log(lite);

    res.send(req.body);
}

exports.get_partyGroup_rpt = async(req, res) => {
    res.send(
        await Party_group.findAll({
            where: {
                financialYearFinancialYearId: req.body.financial_year_id,
                stateStateId: req.body.state_id,
                roundRoundId: req.body.round_id
            },
            include: [{
                model: Party,
                where: {
                    financialYearFinancialYearId: req.body.financial_year_id,
                    stateStateId: req.body.state_id,
                    roundRoundId: req.body.round_id
                },
                include: [{
                    model: Party_lot,
                    where: {
                        // $or:[
                        //     {
                        //         party_lot_status :
                        //         {
                        //             $eq: "ALLOTED"
                        //         }
                        //     },
                        //     {
                        //         party_lot_status :
                        //         {
                        //             $eq: "Luckey Rate"
                        //         }
                        //     }
                        // ]
                        //  party_lot_status : {
                        //     [Op.or]: ['ALLOTED', 'Luckey Rate']
                        //  } 
                        // [Op.or]: [{ 'party_lot_status': "ALLOTED"}, {'party_lot_status': "Luckey Rate"}]
                        // party_lot_status : 'Luckey Rate',
                        // party_lot_status : 'ALLOTED',
                        [Op.or]: [{ party_lot_status: 'ALLOTED' }, { party_lot_status: 'Luckey Rate' }],

                    },
                    include: [Lot],
                    order: [
                        [Party_lot, 'party_lot_rank', 'ASC'],
                        // [Party_lot, 'party_lot_priority', 'ASC'],
                        // [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
                    ],
                }],

            }],

        })
    );
}

exports.get_partyGroup_rpt_result = async(req, res) => {
    res.send(
        await Party_group.findAll({
            where: {
                financialYearFinancialYearId: req.body.financial_year_id,
                stateStateId: req.body.state_id,
                roundRoundId: req.body.round_id
            },
            include: [{
                model: Party,
                where: {
                    financialYearFinancialYearId: req.body.financial_year_id,
                    stateStateId: req.body.state_id,
                    roundRoundId: req.body.round_id
                },
                include: [{
                    model: Party_lot,
                    where: {
                        // $or:[
                        //     {
                        //         party_lot_status :
                        //         {
                        //             $eq: "ALLOTED"
                        //         }

                        //     },
                        //     {
                        //         party_lot_status :
                        //         {
                        //             $eq: "Luckey Rate"
                        //         }
                        //     }
                        // ]
                        //  party_lot_status : {
                        //     [Op.or]: ['ALLOTED', 'Luckey Rate']
                        //  } 
                        // [Op.or]: [{ 'party_lot_status': "ALLOTED"}, {'party_lot_status': "Luckey Rate"}]
                        // party_lot_status : 'Luckey Rate',
                        // party_lot_status : 'ALLOTED',
                        // [Op.or]: [{ party_lot_status: 'ALLOTED' }, { party_lot_status: 'Luckey Rate' }],
                        party_lot_result_status: true,
                    },
                    include: [Lot],
                    // order: [
                    //     [Party_lot, 'party_lot_rank', 'ASC'],
                    //     // [Party_lot, 'party_lot_priority', 'ASC'],
                    //     // [Party_lot, 'party_lot_qty_per_rate', 'ASC'],
                    // ],
                }],

            }],

        })
    );
}

exports.update = async(req, res) => {
    res.send(await Party_group.update(req.body, { where: { party_group_id: req.body.party_group_id } }));
}

exports.add_party_to_group = async(req, res) => {
    try {
        await sequelize.query("INSERT INTO `party_partygroup` (`createdAt`, `updatedAt`, `partyGroupPartyGroupId`, `partyPartyId`) VALUES ('2021-10-13 12:48:46', '2021-10-13 12:48:46', '" + req.body.group.party_group_id + "', '" + req.body.party.party_id + "');");
    } catch (error) {

    }


    res.send(await Party_group.findOne({
        where: {
            party_group_id: req.body.group.party_group_id
        },
        include: [{
            model: Party,
        }],
    }));
}

exports.remove_party_to_group = async(req, res) => {
    await sequelize.query("DELETE FROM `party_partygroup` WHERE `party_partygroup`.`partyGroupPartyGroupId` = " + req.body.group.party_group_id + " AND `party_partygroup`.`partyPartyId` = " + req.body.party.party_id + ";");
    res.send(await Party_group.findOne({
        where: {
            party_group_id: req.body.group.party_group_id
        },
        include: [{
            model: Party,
        }],
    }));
}