// const User = require("../models/users.model.js");
const Farm = require("../models/farm.js");
const Round = require("../models/Round.js");

// Retrieve all User from the database.
exports.load = async (req, res) => {
    res.send(await Farm.findAll());
};


// Update a User identified by the user_id in the request
exports.set = async (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    if (req.body.farmId == undefined) res.send(await Farm.create(req.body));
    else res.send(await Farm.update(req.body, { where: { farmId: req.body.farmId } }));
};