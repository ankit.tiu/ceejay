var mysqldump = require("mysqldump");
exports.backup = async (req, res)=>{
    const result = await mysqldump({
        connection: {
          host: "localhost",
          user: "root",
          password: "",
          database: "ceejay_new",
        },
        dumpToFile: "db_backup/ceejay_new_auto.sql",
      });

   res.send(true);
}