var fs = require('fs');
var parse = require('csv-parse');
const Lot = require("../models/lot");
const Round = require('../models/Round');
const Party = require('../models/Party');
const Party_lot = require('../models/Party_lot');
const sequelize = require("../util/database");
const { Op, QueryTypes } = require("sequelize");
const { log } = require('console');

var csvData = [];
// Lot csv file.
exports.load_csv = async(req, res) => {
    res.send("ture");
    return;
    await fs.createReadStream("uploaded/LOT-CG WITH LAST RESULT.csv")
        .pipe(parse({ delimiter: ',' }))
        .on('data', (csvrow) => csvData.push(csvrow))
        .on('end', async function() {
            //do something with csvData
            csvData.forEach(async x => {
                var lot = {};
                lot.lotNo = x[1];
                lot.lotNameU = x[2];
                lot.lotName = x[3];
                lot.lotPlace = x[4];
                lot.lotGodawn = x[5];
                lot.lotACTB = x[6];
                lot.lotSTANB = x[7];
                lot.lotDISTU = x[8];
                lot.lotOption = x[11];
                lot.lotUpset = x[12];
                lot.lotLRATE = x[13];
                lot.lotWT = x[14];
                lot.lotROUNDPASS = x[15];
                lot.roundRoundId = 1;
                lot.stateStateId = 1;
                console.log(lot);
                //await Lot.save(lot);



            });
            if (res != undefined) res.send(Lot.findAll());

            //   lot.round = await Round.findOne({
            //     where: {
            //         roundId : 1,
            //     }
            //   });
            //   lot.roundRoundId  = Round.findOne({property : [{round_id : 1}]});
            //   console.log( await  Round.findOne({
            //     where: {
            //         roundId : 1,
            //     }
            //   })
            //   );


            //   lot.roundRoundId  = 1;
            //   lot.stateStateId  = 1;



            //   Lot.save(lot);
            //   Lot.setProperties({Round : await  Round.findOne({
            //         where: {
            //             roundId : 1,
            //         }
            //       })});

            //   Lot.Round( await Round.findOne({
            //     where: {
            //         roundId : 1,
            //     }
            //   }));

            //   console.log(csvData[0]);
            //    csvData.forEach(async x => {
            //         // await Lot.save(x);
            //    });
        });

};
// roundRoundId: req.body.round_id, , roundRoundId : {[Op.eq]: null}
exports.get = async(req, res) => {
    res.send(await Lot.findAll({
        where: {stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id},
        order: [
            ["lotInd", 'ASC'],
            ["lotTxt", 'ASC'],
            //[ Lot, 'lotDISTU', 'ASC'],
            // [ Party_lot, 'party_lot_qty_per_rate', 'DESC'],
            // [ Party_lot, 'party_lot_priority', 'ASC'],
            // [ Party, "lotName", "ASC"]
        ],
    }));
};

exports.set = async(req, res) => {

    // for (let index = 0; index < req.body.length; index++) {
    await Lot.update(req.body, { where: { lotId: req.body.lotId } });
    // }
    res.send(true);
}

// exports.indexing = async(req, res) => {
//     var lots = await Lot.findAll({
//         where: { roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
//         order: [
//             // [ Lot, "lotNo"],
//             //[ Lot, 'lotDISTU', 'ASC'],
//             // [ Party_lot, 'party_lot_qty_per_rate', 'DESC'],
//             // [ Party_lot, 'party_lot_priority', 'ASC'],
//             // [ Party, "lotName", "ASC"]
//         ],
//     })

//     for (let index = 0; index < lots.length; index++) {
//         var num = '';
//         var str = '';
//         num += isNumber(lots[index].lotNo.charAt(0)) ? lots[index].lotNo.charAt(0) : '';
//         num += isNumber(lots[index].lotNo.charAt(1)) ? lots[index].lotNo.charAt(1) : '';
//         num += isNumber(lots[index].lotNo.charAt(2)) ? lots[index].lotNo.charAt(2) : '';
//         num += isNumber(lots[index].lotNo.charAt(3)) ? lots[index].lotNo.charAt(3) : '';
//         num += isNumber(lots[index].lotNo.charAt(4)) ? lots[index].lotNo.charAt(4) : '';
//         num = num.trim()

//         str += isNumber(lots[index].lotNo.charAt(0)) ? '' : lots[index].lotNo.charAt(0);
//         str += isNumber(lots[index].lotNo.charAt(1)) ? '' : lots[index].lotNo.charAt(1);
//         str += isNumber(lots[index].lotNo.charAt(2)) ? '' : lots[index].lotNo.charAt(2);
//         str += isNumber(lots[index].lotNo.charAt(3)) ? '' : lots[index].lotNo.charAt(3);
//         str += isNumber(lots[index].lotNo.charAt(4)) ? '' : lots[index].lotNo.charAt(4);
//         str.trim();

//         lots[index].lotInd = parseInt(num);
//         lots[index].lotTxt = str;
//         await Lot.update({ lotInd: parseInt(num), lotTxt: str }, { where: { lotId: lots[index].lotId } });


//         // console.log(lots[index]);


//         // await Lot.update(lots[index], { where: { lotId: lots[index].lotId } });
//     }

//     function isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }

//     res.send(true);

//     // for (let index = 0; index < lots.length; index++) {
//     //     await Lot.update(req.body, { where: { lotId: req.body.lotId } });
//     // }
//     //         await $Lot.set($scope.lots[index]).then(x => $rootScope.show_toest("Indexing"));
// }

exports.indexing = async (req, res) => {
  var lots = await Lot.findAll({
    where: {
      roundRoundId: req.body.round_id,
      stateStateId: req.body.state_id,
      financialYearFinancialYearId: req.body.financial_year_id,
    },
    order: [
      // [ Lot, "lotNo"],
      // [ Lot, 'lotDISTU', 'ASC'],
      // [ Party_lot, 'party_lot_qty_per_rate', 'DESC'],
      // [ Party_lot, 'party_lot_priority', 'ASC'],
      // [ Party, "lotName", "ASC"]
    ],
  });

  for (let index = 0; index < lots.length; index++) {
    var num = "";
    var str = "";
    num += isNumber(lots[index].lotNo.charAt(0))
      ? lots[index].lotNo.charAt(0)
      : "";
    num += isNumber(lots[index].lotNo.charAt(1))
      ? lots[index].lotNo.charAt(1)
      : "";
    num += isNumber(lots[index].lotNo.charAt(2))
      ? lots[index].lotNo.charAt(2)
      : "";
    num += isNumber(lots[index].lotNo.charAt(3))
      ? lots[index].lotNo.charAt(3)
      : "";
    num += isNumber(lots[index].lotNo.charAt(4))
      ? lots[index].lotNo.charAt(4)
      : "";
    num = num.trim();

    str += isNumber(lots[index].lotNo.charAt(0))
      ? ""
      : lots[index].lotNo.charAt(0);
    str += isNumber(lots[index].lotNo.charAt(1))
      ? ""
      : lots[index].lotNo.charAt(1);
    str += isNumber(lots[index].lotNo.charAt(2))
      ? ""
      : lots[index].lotNo.charAt(2);
    str += isNumber(lots[index].lotNo.charAt(3))
      ? ""
      : lots[index].lotNo.charAt(3);
    str += isNumber(lots[index].lotNo.charAt(4))
      ? ""
      : lots[index].lotNo.charAt(4);
    str.trim();

    lots[index].lotInd = parseInt(num);
    lots[index].lotTxt = str;
    await Lot.update(
      { lotInd: parseInt(num), lotTxt: str },
      { where: { lotId: lots[index].lotId } }
    );

    // console.log(lots[index]);

    // await Lot.update(lots[index], { where: { lotId: lots[index].lotId } });
  }

  function isNumber(n) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0);
  }

  // console.log({ roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id-1, party_lot_result_status : true });
  if (req.body.round_id > 1) {
    round_last_year = await Party_lot.findAll({
      where: {
        roundRoundId: req.body.round_id - 1,
        stateStateId: req.body.state_id,
        financialYearFinancialYearId: req.body.financial_year_id,
        party_lot_result_status: true,
      },
      include: [Lot],
    });
    var delete_list = [];
    for (let i = 0; i < round_last_year.length; i++) {
      const element = round_last_year[i].Lot;
      var round_lot = await Lot.findOne({
        where: {
          roundRoundId: req.body.round_id,
          stateStateId: req.body.state_id,
          financialYearFinancialYearId: req.body.financial_year_id,
          lotNo : element.lotNo
        },
      });
      round_lot.destroy();
      //delete_list.push(round_lot);
      //console.log();
      // console.log(element.Lot);
    }
    //console.log(delete_list.length);

    // console.log(round_last_year);
    // round_lot = await Lot.findAll({
    //     where: { roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
    // })
  }

  if (req.body.round_id > 2) {
    round_last_year = await Party_lot.findAll({
      where: {
        roundRoundId: req.body.round_id - 2,
        stateStateId: req.body.state_id,
        financialYearFinancialYearId: req.body.financial_year_id,
        party_lot_result_status: true,
      },
      include: [Lot],
    });
    var delete_list = [];
    for (let i = 0; i < round_last_year.length; i++) {
      const element = round_last_year[i].Lot;
      var round_lot = await Lot.findOne({
        where: {
          roundRoundId: req.body.round_id,
          stateStateId: req.body.state_id,
          financialYearFinancialYearId: req.body.financial_year_id,
          lotNo : element.lotNo
        },
      });
      round_lot.destroy();
      //delete_list.push(round_lot);
      //console.log();
      // console.log(element.Lot);
    }
    //console.log(delete_list.length);

    // console.log(round_last_year);
    // round_lot = await Lot.findAll({
    //     where: { roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
    // })
  }

  if (req.body.round_id > 3) {
    round_last_year = await Party_lot.findAll({
      where: {
        roundRoundId: req.body.round_id - 3,
        stateStateId: req.body.state_id,
        financialYearFinancialYearId: req.body.financial_year_id,
        party_lot_result_status: true,
      },
      include: [Lot],
    });
    var delete_list = [];
    for (let i = 0; i < round_last_year.length; i++) {
      const element = round_last_year[i].Lot;
      var round_lot = await Lot.findOne({
        where: {
          roundRoundId: req.body.round_id,
          stateStateId: req.body.state_id,
          financialYearFinancialYearId: req.body.financial_year_id,
          lotNo : element.lotNo
        },
      });
      round_lot.destroy();
      //delete_list.push(round_lot);
      //console.log();
      // console.log(element.Lot);
    }
    //console.log(delete_list.length);

    // console.log(round_last_year);
    // round_lot = await Lot.findAll({
    //     where: { roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
    // })
  }

  res.send(true);

  // for (let index = 0; index < lots.length; index++) {
  //     await Lot.update(req.body, { where: { lotId: req.body.lotId } });
  // }
  //         await $Lot.set($scope.lots[index]).then(x => $rootScope.show_toest("Indexing"));
};

exports.add_lots = async(req, res) => {
    var file = "";
    if(req.session.master.state_id == 1) file = "uploaded/LOT MP WITH LAST RESULT.csv";
    else file = "uploaded/LOT-CG WITH LAST RESULT.csv";
    await fs.createReadStream(file)
        .pipe(parse({ delimiter: ',' }))
        .on('data', (csvrow) => csvData.push(csvrow))
        .on('end', async function() {
            //do something with csvData
            csvData.forEach(async x => {
                var lot = {};
                lot.lotNo = x[1];
                lot.lotNameU = x[2];
                lot.lotName = x[3];
                lot.lotPlace = x[4];
                lot.lotGodawn = x[5];
                lot.lotACTB = x[6];
                lot.lotSTANB = x[7];
                lot.lotDISTU = x[8];
                lot.lotOption = x[11];
                lot.lotUpset = x[12];
                lot.lotLRATE = x[13];
                lot.lotWT = x[14];
                lot.lotROUNDPASS = x[15];
                lot.financialYearFinancialYearId = req.body.financial_year_id;
                lot.roundRoundId = req.body.round_id;
                lot.stateStateId = req.body.state_id;
                // console.log(lot);
                await Lot.save(lot);
            });
            // if (res != undefined) res.send(Lot.findAll());
            res.send("loading");
        });
    // console.log(req.body);

}

exports.search = async(req, res) => {
  // roundRoundId: req.body.round_id,
    res.send(await Lot.findAll({
        where: {  stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id},
        include: [
            // {
            //   model: Party, attributes: ["party_name"]
            // },
            {
                model: Party_lot,
                where:{ roundRoundId: req.body.round_id},
                attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority", "party_lot_status", "party_lot_rank"],
                include: [Party],
            }
        ],
        attributes: ["lotDISTU", "lotName", "lotId", "lotNo", "lotSTANB", "lotNameU", "lotACTB"],
        order: [
            // [ Lot, "lotNo"],
            ["lotInd", 'ASC'],
            ["lotTxt", 'ASC'],
            [Party_lot, 'party_lot_pur_rate', 'DESC'],
            [Party_lot, 'party_lot_qty_per_rate', 'DESC'],
            [Party_lot, 'party_lot_priority', 'ASC'],
            // [ Party, "lotName", "ASC"]
        ],
    }));
}

exports.balacnce_lot = async(req, res) => {
    var list = await Lot.findAll({
        where: { stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
        order : ['lotInd','lotTxt']
    })
    var list1 = await Lot.findAll({
        where: { stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
        include: [{
            model: Party_lot,
            attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority", "party_lot_status", "party_lot_rank", "party_lot_result_status"],
            where: { party_lot_result_status: true, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
            include: [Party],
            order : ['lotInd','lotTxt']
        }]
    })
    var newlist = [];
    for (let ind = 0; ind < list.length; ind++) {
        const ele = list[ind];

        var flag = true;
        for (let i = 0; i < list1.length; i++) {
            const element = list1[i];
            if (ele.lotId == element.lotId) flag = false;
        }
        if (flag) newlist.push(ele);
    }
    res.send(newlist);
}

exports.DivisionWiseSummaryReports = async(req, res) => {
    var list = await Lot.findAll({
        attributes: ['lotDISTU', [sequelize.fn('sum', sequelize.col('lotLRATE')), 'rate'],
            [sequelize.fn('count', sequelize.col('lotLRATE')), 'lots']
        ],
        where: { roundRoundId: req.body.round_id, stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
        group: ['lotDISTU']
    })

    res.send(list);
}

exports.LotReportWithParty = async(req, res) => {
  //roundRoundId: req.body.round_id, roundRoundId: req.body.round_id,  party_lot_result_status: true,
    var list = await Lot.findAll({
        where: {  stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
        include: [{
            model: Party_lot,
            attributes: ["party_lot_pur_rate", "party_lot_qty_per_rate", "party_lot_priority", "party_lot_status", "party_lot_rank"],
            where: {  stateStateId: req.body.state_id, financialYearFinancialYearId: req.body.financial_year_id },
            include: [Party],
        }],
        order: [
          sequelize.col('lotInd')
        ]
    })

    res.send(list);
}