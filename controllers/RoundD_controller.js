const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const { QueryTypes } = require('sequelize');
// const User = require("../models/users.model.js");
const RoundD = require("../models/RoundD.js");

// Create and Save a new Users
exports.create = async(req, res) => {

    // res.header("Access-Control-Allow-Origin", "*");
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    if (req.body.roundd_id == undefined) res.send(await RoundD.create(req.body));
    else res.send(await RoundD.update(req.body, { where: { roundd_id: req.body.roundd_id } }));

};

// Retrieve all User from the database.
exports.findAll = async(req, res) => {
    // res.send(await RoundD.findAll());
    res.send(await sequelize.query("SELECT * FROM `roundds` LEFT JOIN financial_years ON financial_years.financial_year_id = roundd_year LEFT JOIN states on states.stateId = roundd_state LEFT JOIN rounds on rounds.roundId = round_round_id WHERE 1 order by states.stateId", { type: QueryTypes.SELECT }));
};

// Find a single User with a user_id
exports.findOne = (req, res) => {
    RoundD.findById(req.params.roundd_id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found user with id ${req.params.user_id}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving user with id " + req.params.user_id
                });
            }
        } else res.send(data);
    });
};

exports.delete = async(req, res) => {
    var r = await RoundD.findOne({
        where: {
            roundd_id: req.body.roundd_id
        }
    });
    // console.log(r);
    await r.destroy();
    res.send(true);
};