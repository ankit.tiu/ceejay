const financial_year = require("../models/Financial_year");
const Round = require("../models/Round");
const LotController = require("../controllers/Lot.controller");
const State = require("../models/State");

exports.load = async (req, res)=>{
    await Round.create({"roundName" : "First Round"});
    await Round.create({"roundName" : "Second Round"});
    await Round.create({"roundName" : "Thired Round"});
    await Round.create({"roundName" : "Fouth Round"});
    await financial_year.create({"financial_year_name" : "2019"});
    await financial_year.create({"financial_year_name" : "2020"});
    await financial_year.create({"financial_year_name" : "2021"});
    await State.create({"stateName" : "Madhya Pradesh"});
    await State.create({"stateName" : "Chhattisgarh"});
    // LotController.load_csv();
    res.send("HI");
} 