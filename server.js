const express = require("express");
const bodyParser = require("body-parser");
var session = require('express-session');
const jwt = require("jsonwebtoken");
require("dotenv").config();
const path = require('path');
const Party_lot = require("./models/Party_lot.js");
const User = require("./models/users.js");
const app = express();
app.use(express.static('public'))
app.use(session({ secret: 'XASDASDA' }));
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

let options = {
    dotfiles: "ignore", //allow, deny, ignore
    etag: true,
    extensions: ["htm", "html"],
    index: false, //to disable directory indexing
    maxAge: "7d",
    redirect: false,
    setHeaders: function(res, path, stat) {
        //add this header to all static responses
        res.set("x-timestamp", Date.now());
    }
};

app.use(express.static("app/public", options));
// app.use(express.json());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/Apis", (req, res) => {
    res.json({ message: "Welcome to bezkoder application." });
});

app.get('/Login', async(request, response) => {
    if (request.session.user == undefined) response.sendFile('./public/Views/login.html', { root: __dirname });
    else response.redirect('/');
});

app.post('/Login', async(request, response) => {
    if (request.body.user_name != undefined && request.body.userUserpassword != undefined && request.body.user_name != "" && request.body.userUserpassword != "") {
        // await User.create({ userUsername: request.body.user_name, userUserpassword: request.body.userUserpassword });
        // console.log(await User.findAll());
        var r = await User.findOne({
            where: {
                userUsername: request.body.user_name,
                userUserpassword: request.body.userUserpassword
            }
        });

        // console.log(r);

        if (r != null) {
            if (r.userId == undefined) response.sendFile('./public/Views/login.html', { root: __dirname });
            else {
                if (request.session.user == undefined) request.session.user = r;
                response.redirect('/');
                // response.sendFile('./public/Views/index.html', { root: __dirname });
            }
        } else {
            response.redirect('/Login');
        }
    } else response.sendFile('./public/Views/login.html', { root: __dirname });
});

app.get('/', function(request, response) {
    if (request.session.user === undefined) response.redirect('/login');
    else response.sendFile('./public/Views/index.html', { root: __dirname });
});

app.get('/logout', function(request, response) {
    request.session.user = undefined;
    response.redirect('/login');
});

app.get('/test', authenticateToken, function(req, res) {
    var user = req.user;
    res.json(user);
});

function authenticateToken(req, res, nex) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sensdStatus(401);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) return res.sensdStatus(403);
        req.user = user;
        nex();
    });
}

require("./routes/Apis.routes.js")(app);

// set port, listen for requests
app.listen(process.env.PORT || 4000, () => {
    console.log(`Server is running on port ${process.env.PORT || 4000}.`);
});