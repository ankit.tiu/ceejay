var CalculatorService = angular
    .module("ConnectToServer", [])
    .service("$myhttp", function($http, $rootScope, $mdDialog, $mdToast) {
        this.http = $http;
        this.scope = {};

        this.get = (path, query) => {
            if (loading == 0) {
                loading = loading + 1;
                $rootScope.loading = true;
            }
            return this.http
                .get(BaseUrl + path, query === undefined ? null : query)
                .then((data, status, headers, config) => {
                    loading--;
                    if (loading == 0) $rootScope.loading = false;
                    return data.data;
                    // if (data.data.status == 200)
                    //     return data.data.total_no_of_row !== undefined ?
                    //         $(document).trigger(
                    //             "total_no_of_rowEvent",
                    //             data.data.total_no_of_row
                    //         ) :
                    //         data.data.data;
                    // if (data.data.status == 300)
                    //     window.location.replace(BaseUrl + "logout");
                    //if (data.data.status == 400) $(document).trigger("recordPresentError");
                });
        };
        this.post = function(path, query) {
            if (loading == 0) {
                loading = loading + 1;
                $rootScope.loading = true;
            }
            return this.http
                .post(BaseUrl + path, query === undefined ? null : query)
                .then(function(data, status, headers, config) {
                    loading--;
                    if (loading == 0) $rootScope.loading = false;
                    return data.data;
                    // if (data.data.status == 200)
                    //     return data.data.total_no_of_row !== undefined ?
                    //         $(document).trigger(
                    //             "total_no_of_rowEvent",
                    //             data.data.total_no_of_row
                    //         ) :
                    //         data.data.data;
                    // if (data.data.status == 300)
                    //     window.location.replace(BaseUrl + "logout");
                    //if (data.data.status == 400) $(document).trigger("recordPresentError");
                });
        };
        this.redirectPost = function(url, data) {
            var form = document.createElement("form");
            document.body.appendChild(form);
            form.method = "post";
            form.action = url;
            for (var name in data) {
                var input = document.createElement("input");
                input.type = "hidden";
                input.name = name;
                input.value = data[name];
                form.appendChild(input);
            }
            form.submit();
        };

        $rootScope.alert = (title, content, lable, buttonText = "Ok", ev) => {
            $mdDialog.show(
                $mdDialog
                .alert()
                .parent(angular.element(document.querySelector("#popupContainer")))
                .clickOutsideToClose(true)
                .title(title)
                .textContent(content)
                .ok(buttonText)
                .targetEvent(ev)
            );
        };

        $rootScope.error = (msg) => $rootScope.alert("Error", msg);

        $rootScope.showConfirm = (
                title = "Would you like to delete your debt?",
                content = "All of the banks have agreed to forgive you your debts.",
                lable,
                okButtonText = "Please do it!",
                cancleButtonText = "Cancel",
                ev
            ) =>
            $mdDialog.show(
                $mdDialog
                .confirm()
                .title(title)
                .textContent()
                .targetEvent(ev)
                .ok(okButtonText)
                .cancel(cancleButtonText)
            );
        $rootScope.Toest = (msg) =>
            $mdToast.show(
                $mdToast
                .simple()
                .textContent(msg)
                .highlightAction(true)
                .highlightClass("md-accent")
                .position("bottom right")
            );
        // $mdDialog.show(confirm).then(function(result) {
        //   $scope.status = 'You decided to name your dog ' + result + '.';
        // }, function() {
        //   $scope.status = 'You didn\'t name your dog.';
        // });
        $rootScope.prompt = (
                ev = null,
                title = "What would you name your dog?",
                Content = "Bowser is a common name.",
                placeholder = "---- ----",
                Label = "Dog name",
                Value = "",
                ok = "Okay!",
                cancel = "Cancel"
            ) =>
            $mdDialog.show(
                $mdDialog
                .prompt()
                .title(title)
                .textContent(Content)
                .placeholder(placeholder)
                .ariaLabel(Label)
                .initialValue(Value)
                .targetEvent(ev)
                .required(true)
                .ok(ok)
                .cancel(cancel)
            );
    });
CalculatorService.service("$User", function($myhttp) {
    this.load = () => { return $myhttp.get("Apis/Users") };
    this.backup = () => $myhttp.get("Apis/backup");
    this.set = (x) => $myhttp.post("Apis/Users", x);

    this.set_session = (x) => $myhttp.post("Apis/Users/set_settion", x);
    this.get_session = () => $myhttp.get("Apis/Users/get_settion");

    this.get_by_id = (x) => { return $myhttp.get("Apis/Users/" + x) };

    this.getMe = () => {
        return $myhttp.get("Apis_v1/user/getMe");
    };

    this.get_all_user = () => {
        return $myhttp.get("Apis_v1/user/get_all_user");
        //            return $http.get(BaseUrl + "Apis_v1/user/get_all_user").then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.get_user_by_id = function(id) {
        return $myhttp.get("Apis_v1/user/get_user_by_id?user_id=" + id);
        //            return $http.get(BaseUrl + "Apis_v1/user/get_user_by_id?user_id="+id).then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.get_all_teamlead = function(id) {
        return $myhttp.get("Apis_v1/user/get_all_teamlead");
        //            return $http.get(BaseUrl + "Apis_v1/user/get_all_teamlead").then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.get_all_employee = (x) => {
        return $myhttp.post("Apis_v1/user/get_all_employee", x);
        //            return $http.post(BaseUrl + "Apis_v1/user/get_all_employee",x).then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.get_all_employee_for_admin = (x) => {
        return $myhttp.post("Apis_v1/user/get_all_employee_for_admin", x);
        //            return $http.post(BaseUrl + "Apis_v1/user/get_all_employee_for_admin",x).then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    //get_other_all_employee
    this.get_other_all_employee = (x) => {
        return $myhttp.post("Apis_v1/user/get_other_all_employee", x);
        //            return $http.post(BaseUrl + "Apis_v1/user/get_other_all_employee",x).then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    //set_employee
    this.set_employee = (x) => {
        return $myhttp.post("Apis_v1/user/set_employee", x);
        //            return $http.post(BaseUrl + "Apis_v1/user/set_employee",x).then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.get_dashbord = (x) => {
        return $myhttp.post("Apis_v1/user/get_dashbord", x);
        //            return $http.post(BaseUrl + "Apis_v1/user/get_dashbord",x).then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.get_temlead_employ = (x) => {
        return $myhttp.get("Apis_v1/user/get_temlead_employ", x);
        //            return $http.get(BaseUrl + "Apis_v1/user/get_temlead_employ").then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
    };

    this.updatePassword = (x) => {
        return $myhttp.post("Apis_v1/user/updatePassword", x);
    };

    this.getUserFull = (x) => {
        return $myhttp.post("Apis_v1/user/getUserFull", x);
    };
});

CalculatorService.service("$Group", function($myhttp) {
    this.load = () => { return $myhttp.get("Apis/Group") };
    this.get_by_id = (x) => { return $myhttp.get("Apis/Group/" + x) };
});

CalculatorService.service("$Round", function($myhttp) {
    this.load = () => { return $myhttp.get("Apis/Round") };
    this.get_by_id = (x) => { return $myhttp.get("Apis/Round/" + x) };
    this.set = (x) => $myhttp.post("Apis/Round/", x);
});

CalculatorService.service("$FinancialYear", function($myhttp) {
    this.load = () => { return $myhttp.get("Apis/FinancialYear") };
    this.get_by_id = (x) => { return $myhttp.get("Apis/FinancialYear/" + x) };
    this.set = (x) => $myhttp.post("Apis/FinancialYear/", x);
    this.refresh = (x) => $myhttp.post("Apis/FinancialYear/refresh", x);
});

CalculatorService.service("$State", function($myhttp) {
    this.load = () => { return $myhttp.get("Apis/State") };
    this.get_by_id = (x) => { return $myhttp.get("Apis/State/" + x) };
});

CalculatorService.service("$Party", function($myhttp) {
    // this.load = (x) => { return $myhttp.get("Apis/Party", x)};
    this.load = (x) => { return $myhttp.post("Apis/Party", x) };
    this.load_focast = (x) => { return $myhttp.post("Apis/Party/load_focast", x) };
    this.get_by_id = (x) => { return $myhttp.get("Apis/Party/" + x) };
    this.set = (x) => { return $myhttp.post("Apis/Party/set", x) };
    this.set_round_party = (x) => $myhttp.post("Apis/Party/set_round_party", x);
    this.get_party_by_round = (x) => $myhttp.post("Apis/Party/get_party_by_round", x);
    this.set_party_lot = (x) => $myhttp.post("Apis/Party/set_party_lot", x);
    this.loadpartywise = (x) => $myhttp.post("Apis/Party/loadpartywise", x);
    this.capacity = (x) => $myhttp.post("Apis/Party/capacity", x);
    this.allocateRank = (x) => $myhttp.post("Apis/Party/allocateRank", x);
    this.allocateLot = (x) => $myhttp.post("Apis/Party/allocateLot", x);
    this.removeallocateLot = (x) => $myhttp.post("Apis/Party/removeallocateLot", x);
    this.update_party_lot_data = (x) => $myhttp.post("Apis/Party/update_party_lot_data", x);
    this.get_party_wise_summery = (x) => $myhttp.post("Apis/Party/get_party_wise_summery", x);
    this.loadpartywise_reslut = (x) => $myhttp.post("Apis/Party/loadpartywise_reslut", x);
    this.delete_all_loats = (x) => $myhttp.post("Apis/Party/delete_all_loats", x);
});

CalculatorService.service("$Lot", function($myhttp) {
    this.load = x => $myhttp.post("Apis/Lot", x);
    this.add_lots = x => $myhttp.post("Apis/add_lots", x);
    this.search = x => $myhttp.post("Apis/Lot/search", x);
    this.set = x => $myhttp.post("Apis/Lot/set", x);
    this.set_index = x => $myhttp.post("Apis/Lot/indexing", x);
    this.balance_lot = x => $myhttp.post("Apis/Lot/balance_lot", x);
    this.DivisionWiseSummaryReports = x => $myhttp.post("Apis/Lot/DivisionWiseSummaryReports", x);
    this.LotReportWithParty = x => $myhttp.post("Apis/Lot/LotReportWithParty", x);
});

CalculatorService.service("$PartyGroup", function($myhttp) {
    this.get_partyGroup = x => $myhttp.post("Apis/PartyGroup", x);
    this.set_partyGroup = x => $myhttp.post("Apis/PartyGroupNew", x);
    this.get_partyGroup_rpt = x => $myhttp.post("Apis/PartyGroup/get_partyGroup_rpt", x);
    this.get_partyGroup_rpt_result = x => $myhttp.post("Apis/PartyGroup/get_partyGroup_rpt_result", x);
    this.add_partyGroup = x => $myhttp.post("Apis/PartyGroup/add_partyGroup", x);
    this.add_party_to_group = x => $myhttp.post("Apis/PartyGroup/add_party_to_group", x);
    this.remove_party_to_group = x => $myhttp.post("Apis/PartyGroup/remove_party_to_group", x);
    this.update = x => $myhttp.post("Apis/PartyGroup/update", x);
});

CalculatorService.service("$farm", function($myhttp) {
    this.load = x => $myhttp.get("Apis/farm");
    this.set = x => $myhttp.post("Apis/farm", x);
});

CalculatorService.service("$roundD", function($myhttp) {
    this.load = x => $myhttp.get("Apis/RoundD");
    this.set = x => $myhttp.post("Apis/RoundD", x);
    this.destoy = x => $myhttp.post("Apis/RoundD/destoy", x);
});


var CalculatorService = angular
    .module("keyword", [])
    .service("$key", function($http) {
        this.onSave = function(callback) {
            Mousetrap.bind("ctrl+s", function(e) {
                e.returnValue = false;
                callback(e);
            });
        };

        this.onFunctionKey = function(callback) {
            Mousetrap.bind("f1", function(e) {
                e.returnValue = false;
                callback("f1", e);
            });

            Mousetrap.bind("f2", function(e) {
                e.returnValue = false;
                callback("f2", e);
            });

            Mousetrap.bind("f3", function(e) {
                e.returnValue = false;
                callback("f3", e);
            });

            Mousetrap.bind("f4", function(e) {
                e.returnValue = false;
                callback("f4", e);
            });

            Mousetrap.bind("f5", function(e) {
                e.returnValue = false;
                callback("f5", e);
            });

            Mousetrap.bind("f6", function(e) {
                e.returnValue = false;
                callback("f6", e);
            });

            Mousetrap.bind("f7", function(e) {
                e.returnValue = false;
                callback("f7", e);
            });

            Mousetrap.bind("f8", function(e) {
                e.returnValue = false;
                callback("f8", e);
            });
        };

        this.onSearch = function(callback) {
            Mousetrap.bind("ctrl+f", function(e) {
                e.returnValue = false;
                callback();
            });
        };

        this.setKey = function(key, callback) {
            Mousetrap.bind(key, function(e) {
                e.returnValue = false;
                callback();
            });
        };

        //        this.load = ()=>{
        //            return $http.get(BaseUrl + "Apis_v1/Specializion/getSpecializion").then(function (data, status, headers, config) {
        //                if (data.data.status == 200) return data.data.data;
        //            });
        //        };
        //
        //        this.set = (x)=>{
        //            return $http.post(BaseUrl + "Apis_v1/Specializion/setSpecializion",x).then(function (data, status, headers, config){
        //                if (data.data.status == 200) return data.data.data;
        //            });
        //        };
    });