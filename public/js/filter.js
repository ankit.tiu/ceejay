app.filter('keyboardShortcut', function($window) {
    return function(str) {
        if (!str) return;
        var keys = str.split('-');
        var isOSX = /Mac OS X/.test($window.navigator.userAgent);

        var separator = (!isOSX || keys.length > 2) ? '+' : '';

        var abbreviations = {
            M: isOSX ? '⌘' : 'Ctrl',
            A: isOSX ? 'Option' : 'Alt',
            S: 'Shift'
        };

        return keys.map(function(key, index) {
            var last = index === keys.length - 1;
            return last ? key : abbreviations[key];
        }).join(separator);
    };
})

app.filter('decimalRound', function() {
    return function(input) {
        return parseInt(input).toFixed(2);
    };
});

app.filter('spaceFlt', function() {
    return function(input) {
        if(input == "Luckey Rate") return "L.Rate"
        else if(input == "CAP. OVER") return "CAP.OV"
        else if(input == "ALLOTED") return "ALLOTED"
        else return "";
        return parseInt(input).toFixed(2);
    };
});

app.filter('INR', function() {
    return function(input) {
        if (!isNaN(input)) {
            if(input == null) return 0;
            // var currencySymbol = '₹';
            var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return currencySymbol + output;
        }
    }
});