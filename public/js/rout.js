app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "Views/dashbord.html",
            controller: "dashbordCtrl"
        })
        .when("/users", {
            templateUrl: "Views/user.html",
            controller: "userCtrl"
        })
        .when("/Tander", {
            templateUrl: "Views/Tander.html",
            controller: "tanderCtrl"
        })
        .when("/OpenRound", {
            templateUrl: "Views/OpenRound.html",
            controller: "openRoundCtrl"
        })
        .when("/ExpectedResult", {
            templateUrl: "Views/ExpectedResult.html",
            controller: "expectedResultCtrl"
        })
        .when("/Result", {
            templateUrl: "Views/Result.html",
            controller: "resultCtrl"
        })
        .when("/years", {
            templateUrl: "Views/YearsTemplate.html",
            controller: "yearCtrl"
        })
        .when("/round", {
            templateUrl: "Views/round.html",
            controller: "roundCtrl"
        })
        .when("/Lot", {
            templateUrl: "Views/Lot.html",
            controller: "lotCtrl"
        })
        .when("/partywise", {
            templateUrl: "Views/partywise.html",
            controller: "partywiseCtrl"
        })
        .when("/partywise:RptType=rpt", {
            templateUrl: "Views/partywise.html",
            controller: "partywiseCtrl"
        })
        .when("/lotwise", {
            templateUrl: "Views/lotwise.html",
            controller: "lotwiseCtrl"
        })
        .when("/lotwiseReport", {
            templateUrl: "Views/lotwiseReport.html",
            controller: "lotwiseReportCtrl"
        })
        .when("/capacity", {
            templateUrl: "Views/capacity.html",
            controller: "capacityCtrl"
        })
        .when("/forcast", {
            templateUrl: "Views/forcast.html",
            controller: "forcastCtrl"
        })
        .when("/partygroup", {
            templateUrl: "Views/partygroup.html",
            controller: "partygroupCtrl"
        })
        .when("/groupWiseReport", {
            templateUrl: "Views/groupWiseReport.html",
            controller: "groupWiseReportCtrl"
        })
        .when("/partywisesummryreports", {
            templateUrl: "Views/partywisesummryreports.html",
            controller: "partywisesummryreportsCtrl"
        })
        .when("/DivisionWiseSummaryReports", {
            templateUrl: "Views/DivisionWiseSummaryReports.html",
            controller: "DivisionWiseSummaryReportsCtrl"
        })
        .when("/LotReportWithParty", {
            templateUrl: "Views/LotReportWithParty.html",
            controller: "LotReportWithPartyCtrl"
        })
        .when("/lotwiseSinglePartyReport", {
            templateUrl: "Views/lotwiseSinglePartyReport.html",
            controller: "lotwiseSinglePartyReportCtrl"
        })
        .when("/groupWiseSummaryReport", {
            templateUrl: "Views/groupWiseSummaryReport.html",
            controller: "groupWiseSummaryReportCtrl"
        })
        .when("/ResultSummaryGroupWise", {
            templateUrl: "Views/ResultSummaryGroupWise.html",
            controller: "groupWiseSummaryReportCtrl"
        })
        .when("/resultEntry", {
            templateUrl: "Views/resultEntry.html",
            controller: "resultEntryCtrl"
        })
        .when("/farm", {
            templateUrl: "Views/farm.html",
            controller: "farmCtrl"
        })
        .when("/balancLotReport", {
            templateUrl: "Views/lotwiseReport.html",
            controller: "lotwiseReportCtrl"
        })
        .when("/partydisplayname", {
            templateUrl: "Views/partydisplayname.html",
            controller: "partydisplaynameCtrl"
        })
        .when("/Tanderdate", {
            templateUrl: "Views/Tanderdate.html",
            controller: "TanderdateCtrl"
        })
        // .when("/vehicleData", {
        //     templateUrl: "vehicleData",
        //     controller: "vehicleDataCtrl"
        // })
        // .when("/vehicleRide", {
        //     templateUrl: "vehicleRide",
        //     controller: "vehicleRideCtrl"
        // })
        // .when("/vehicle:vehice_id", {
        //     templateUrl: "vehicle",
        //     controller: "vehicleCtrl"
        // })
});