app.controller("appCtrl", ($scope, $rootScope, $mdDialog, $mdToast, $Round, $FinancialYear, $State, $User, $farm, $roundD) => {
    $scope.title = "Ceejay";
    $rootScope.subtitle = "Dashbord";
    $rootScope.Tdate = new Date();
    $rootScope.BaseUrl = "http://localhost:4000";
    $scope.farm = {};


    $rootScope.Round = [];
    $rootScope.Roundd = [];
    $rootScope.loading = false;
    $rootScope.viewType = 1;
    $rootScope.master = {};

    $User.get_session().then(x => $rootScope.master = x);
    $farm.load($rootScope.master).then(x => $scope.farm = x.length == 0 ? {} : x[0]);

    $rootScope.set_session = () => $User.set_session($rootScope.master).then(x => $rootScope.master = x);

    // $rootScope.master.financial_year_id = 1;
    // $rootScope.master.state_id = 1;
    // $rootScope.master.round_id = 1;

    // $rootScope.years = ['2019', '2020', "2021"];
    // $rootScope.master.year = '2019';
    // $rootScope.states = ['Madhya Pradesh', 'Chhattisgarh'];
    // $rootScope.master.state = 'Madhya Pradesh';

    // $rootScope.rounds = ['1 Ground', '2 Ground', '3 Ground', '4 Ground'];
    // $rootScope.master.round = '1 Ground';

    $Round.load().then(x => $rootScope.Round = x);
    $FinancialYear.load().then(x => $rootScope.years = x);
    $State.load().then(x => $rootScope.states = x);
    $roundD.load().then(x => $rootScope.Roundd = x);

    $scope.backUpDB = () => {
        $User.backup().then(x => {
            $rootScope.show_msg("Backup Done.");
        });
    }


    $rootScope.get_heading = () => {
        if ($rootScope.Roundd.find(x => x.round_round_id == $rootScope.master.round_id && x.roundd_state == $rootScope.master.state_id && x.roundd_year == $rootScope.master.financial_year_id) != undefined)
            return `( ${($rootScope.states.find(x => x.stateId == $rootScope.master.state_id)).stateName} Tender - ${($rootScope.years.find(x => x.financial_year_id == $rootScope.master.financial_year_id)).financial_year_name}) Round : ${$rootScope.master.round_id} Dt: ${$rootScope.Roundd.find(x => x.round_round_id == $rootScope.master.round_id && x.roundd_state == $rootScope.master.state_id && x.roundd_year == $rootScope.master.financial_year_id).roundd_date}`;
        else
            return `( ${($rootScope.states.find(x => x.stateId == $rootScope.master.state_id)).stateName} Tender - ${($rootScope.years.find(x => x.financial_year_id == $rootScope.master.financial_year_id)).financial_year_name}) Round : ${$rootScope.master.round_id}`;
    }

    $rootScope.get_heading_1 = () => {
        if ($rootScope.Roundd.find(x => x.round_round_id == $rootScope.master.round_id && x.roundd_state == $rootScope.master.state_id && x.roundd_year == $rootScope.master.financial_year_id) != undefined)
            return `( ${($rootScope.states.find(x => x.stateId == $rootScope.master.state_id)).stateName} Tender - ${($rootScope.years.find(x => x.financial_year_id == $rootScope.master.financial_year_id)).financial_year_name}) Round : ${$rootScope.master.round_id} Dt: ${$rootScope.Roundd.find(x => x.round_round_id == $rootScope.master.round_id && x.roundd_state == $rootScope.master.state_id && x.roundd_year == $rootScope.master.financial_year_id).roundd_date}`;
        else
            return `( ${($rootScope.states.find(x => x.stateId == $rootScope.master.state_id)).stateName} Tender - ${($rootScope.years.find(x => x.financial_year_id == $rootScope.master.financial_year_id)).financial_year_name})`;
    }

    $rootScope.show_msg = (msg, title) => {
        $mdToast.show($mdToast.simple().textContent(msg)
            // .position(pinTo)
            .hideDelay(3000));
        //   .then(function() {
        //     $log.log('Toast dismissed.');
        //   }).catch(function() {
        //     $log.log('Toast failed or was forced to close early by another toast.');
        //   });
    }



    $scope.get_date = () => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const dateObj = new Date();
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, '0');
        const year = dateObj.getFullYear();
        const output = month + '\n' + day + ',' + year;

        return output;
    };

    $scope.onChangeView = () => {
        if ($rootScope.viewType == 1) $rootScope.viewType = 2;
        else if ($rootScope.viewType == 2) $rootScope.viewType = 3;
        else if ($rootScope.viewType == 3) $rootScope.viewType = 1;
    };

    $rootScope.show_alert = (msg, ev) => {

        if (ev == undefined)
            $mdDialog.show(
                $mdDialog.alert()
                    // .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Warning')
                    .textContent(msg)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Got it!')
            );
        else
            $mdDialog.show(
                $mdDialog.alert()
                    // .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Warning')
                    .textContent(msg)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Got it!')
                    .targetEvent(ev)
            );
    }

    $rootScope.show_toest = (msg, ev) => {
        $mdToast.show(
            $mdToast.simple()
                .textContent(msg)
                // .position(pinTo)
                .hideDelay(3000))
        // .then(function () {
        //     $log.log('Toast dismissed.');
        // }).catch(function () {
        //     $log.log('Toast failed or was forced to close early by another toast.');
        // });
    }

    $rootScope.show_dialog = (title, msg, ev, success) => {
        var confirm = $mdDialog.confirm()
            .title(title,)
            .textContent(msg)
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('yes')
            .cancel('no');

        $mdDialog.show(confirm).then(success, () => {
            $mdToast.show($mdToast.simple().textContent('You Cancel').hideDelay(3000));
        });
    }

    $scope.openMenu = function ($mdMenu, ev) {
        originatorEv = ev;
        $mdMenu.open(ev);
    };

    $scope.btnPassword = (ev) => {
        var confirm = $mdDialog.prompt()
            .title('Kindly reset password?')
            .textContent('New Password.')
            .placeholder('New Password')
            .ariaLabel('Dog name')
            // .initialValue('Buddy')
            .targetEvent(ev)
            .required(true)
            .ok('Okay!')
            .cancel('Cancel');

        $mdDialog.show(confirm).then(async (result) => {
            // $scope.status = 'You decided to name your dog ' + result + '.';
            $scope.user.new_password = result;
            await $Users.reset_password($scope.user);
            $rootScope.Toest("Saved.");
            $scope.reset();
        },
            function () {
                $rootScope.Toest("Cancel.");
            });
    };

    $scope.onHelp = () => $scope.$broadcast('onHelp');
    $scope.onRefresh = () => $scope.$broadcast('onRefresh');
    $scope.onAdd = () => $scope.$broadcast('onAdd');
    $scope.onSearch = () => $scope.$broadcast('onSearch');
    $scope.onBack = () => $scope.$broadcast('onBack');
    $scope.onClear = () => $scope.$broadcast('onClear');
    $scope.onDownload = () => $scope.$broadcast('onDownload');

    $rootScope.load_url = (x, y) => {
        if (y == undefined) window.open($rootScope.BaseUrl + "/#!/" + x, "_self");
        else window.open($rootScope.BaseUrl + "/" + x, "_self");
    };

    $scope.$on("onLoading", (x) => {
        $scope.loading = x;
    });
});

app.controller('userCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $User) {
    $rootScope.title = "User";
    $scope.reset = () => {
        $scope.$broadcast('onLoading', {
            loading: true
        });
        $rootScope.subtitle = "Users";
        $scope.users = [];
        $scope.user = {};
        $scope.search = {};
        $scope.action = 1;
        $scope.searchView = 1;
        // $scope.viewType = 1;


        if ($routeParams.user_id != undefined) {
            $scope.action = 2;
            $User.get_by_id($routeParams.user_id).then(x => $scope.user = x);
        } else if ($routeParams.user_id == "") {
            $scope.user = {};
        } else $User.load().then(x => $scope.users = x);
    }

    $scope.reset();

    $scope.add_user = (ev) => {

        // if ($scope.user.user_name == "" || $scope.user.user_name == undefined) return $rootScope.show_alert("Kindly enter name.", ev);
        // if ($scope.user.phone == "" || $scope.user.phone == undefined || $scope.user.phone <= 0) return $rootScope.show_alert("Kindly enter proper phone number.", ev);

        // if ($scope.user.email == "" || $scope.user.email == undefined) return $rootScope.show_alert("Kindly enter email", ev);

        // if ($scope.user.username == "" || $scope.user.username == undefined) return $rootScope.show_alert("Kindly enter username", ev);

        // if ($scope.user.password == "" || $scope.user.password == undefined) return $rootScope.show_alert("Kindely enter password", ev);


        $rootScope.show_dialog('do you want to save this entry', "message", ev, () => {
            $User.set($scope.user);
            // $scope.users.push($scope.user);
            // $scope.user = {};
            $scope.reset();
        });


        return;
    }

    $scope.edit_user = (x) => {
        $scope.user = x;
        $scope.action = 2;
    }

    $scope.add_new = () => {
        $scope.action = 2;
        alert("add new called");
        $scope.user = {};
    }

    $scope.$on("onRefresh", () => window.open("http://localhost:3000/#!/user", "_self"));
    // $scope.reset());
    $scope.$on("onAdd", () => window.open("http://localhost:3000/#!/user?user_id=", "_self"));
    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);

});

app.controller('groupCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Group) {
    $rootScope.title = "Group";

    $scope.reset = () => {
        $scope.title = "";
        $rootScope.subtitle = "group"
        $scope.groups = [];
        $scope.group = {};
        $scope.search = {};
        $scope.action = 1;
        $scope.searchView = 1;


        if ($routeParams.group_id != undefined) {
            $scope.action = 2;
            $Group.get_by_id($routeParams.user_id).then(x => $scope.group = x);
        } else if ($routeParams.group_id == "") {
            $scope.group = {};
        } else $Group.load().then(x => $scope.group = x);
    }

    $scope.reset();

    $scope.add_group = (ev) => {

        if ($scope.group.groupid == "" || $scope.group.groupid == undefined || $scope.group.groupid <= 0) {
            $rootScope.show_alert("Please enter Group ID", ev);
            return;
        }
        if ($scope.group.group_name == "" || $scope.group.group_name == undefined) {
            $rootScope.show_alert("Please enter Group", ev);
            return;
        }


        $rootScope.show_dialog('do you want to save this entry', "message", ev, () => {
            $scope.groups.push($scope.group);
            $scope.group = {};
            $scope.action = 1;
        });

    }




    $scope.edit_group = (x) => {
        $scope.group = x;
        $scope.action = 2;
    }


    $scope.add_new = () => {
        $scope.action = 2;
        $scope.group = {};
    }

    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);


});

app.controller('tanderCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Party) {
    $rootScope.title = "Tander";
    $scope.mainTable = "";
    $scope.party_list = [];
    $scope.view_table = true;
    $scope.tander = "";
    $scope.fileContent = "ankti";


    $scope.reset = () => {
        $rootScope.subtitle = "Tander";
        // $scope.vehicles = [];
        // $scope.vehicle = {};
        // $scope.search = {};
        $scope.action = 1;
        $scope.searchView = 1;
        $scope.tander = "";
        $scope.mainTable = "";
        $scope.party_list = [];
        $scope.view_table = true;
        $scope.tander = "";
        // if ($routeParams.vehice_id != undefined) {
        //     $scope.action = 2;
        //     $Vehicle.get_by_id($routeParams.vehice_id).then(x => $scope.vehicle = x);
        // } else if ($routeParams.vehice_id == "") {
        //     $scope.vehicle = {};
        // } else $Vehicle.load().then(x => $scope.vehicle = x);
    }

    $scope.tander_text = (x) => {

        $("#tander_view").html(x);

        // $("#tander_view table tr").each((index, tr) => {
        //     if (index == 1) $scope.mainTable = $(tr.cells[1]).children()[0];
        // });
        //for (let index = 0; index < $scope.mainTable.rows.length; index++)  $scope.party_list.push($scope.mainTable.rows[index].cells[0].innerHTML.trim().replace("*", "").trim());

        $scope.mainTable = $("#tander_view table tr");
        for (let index = 0; index < $scope.mainTable.length; index++) $scope.party_list.push($scope.mainTable[index].cells[0].innerHTML.trim().replace("*", "").trim());
        $scope.view_table = false;

        // $scope.mainTable = $("#tander_view table tr");
        // for (let index = 0; index < $scope.mainTable.length; index++) $scope.party_list.push($scope.mainTable[index].cells[0].innerHTML.trim().replace("*", "").trim());
        // $scope.view_table = false;
    }

    $scope.reset();

    $scope.btnContinew = async (ev) => {


        var confirm = $mdDialog.confirm()
            .title('Would you like to add all this entry?')
            .textContent('All the ' + $scope.party_list.length + " will be added  to application")
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Please do it!')
            .cancel('No Thanks');

        $mdDialog.show(confirm).then(async () => {
            $rootScope.show_toest("Addeding", ev);
            $row = await $Party.set_round_party({ "party_list": $scope.party_list, "master": $rootScope.master });
            if ($row.action) {
                $rootScope.show_toest("Added", ev);
                $scope.reset();
                $scope.tander = "";
            }
        }, function () {
            $rootScope.show_toest("You Cancel")
        });
    }

    $scope.fileChange = (x) => {
        x.forEach(element => {
            $scope.party_list.push(element.trim().replace("*", "").trim());
        });
    }
    $scope.$on("onBack", () => history.back());
    $scope.$on("onAdd", () => $scope.tander = "");
    // $scope.$on("onRefresh", () => {
    //     console.log("HI");
    //     $scope.tander = "";
    // });
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);
});

app.controller('openRoundCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Party) {
    $rootScope.title = "Open Round";
    $rootScope.subtitle = "Open Round";
    $scope.view_table = true;
    $scope.row = {};
    $scope.row.party = {};
    $scope.tander = {};
    $scope.tander = "";

    $scope.tander_text = (x) => {
        $("#tander_view").html(x);
        $("#tander_view table tr").each((index, tr) => {
            if (index == 0) {
                // console.log( $(tr.cells[0]).find("a").text().replace("M/s ", ""));
                // console.log($(tr.cells[0]).find("a").text().replace("Mrs. ", "").replace("&                  ", "&amp; ").replace("Mr. ", "").replace("M/S ", "").replace("M/s ", "").replace("M/S ", "").replace("M/s ", "").trim());
                // console.log($(tr.cells[0]).find("a").text().replace("Mrs. ", "").replace("&                  ", "&amp; ").replace("Mr. ", "").replace("M/S ", "").replace("M/s ", "").replace("M/S ", "").replace("M/s ", "").replace("MR. ", "").replace("  ", "").replace("TRADINGCO", "TRADING CO").trim());
                if ($scope.Party.find((o) => { return o.party_name.replace("M/S ", "") === $(tr.cells[0]).find("a").text().replace("Mrs. ", "").replace("&                  ", "&amp; ").replace("Mr. ", "").replace("M/S ", "").replace("M/s ", "").replace("M/S ", "").replace("M/s ", "").replace("MR. ", "").replace("               ", "").replace("TRADINGCO", "TRADING CO").trim() }) != undefined) {
                    $scope.row.party = $scope.Party.find((o) => { return o.party_name.replace("M/S ", "") === $(tr.cells[0]).find("a").text().replace("Mrs. ", "").replace("&                  ", "&amp; ").replace("Mr. ", "").replace("M/S ", "").replace("M/s ", "").replace("M/S ", "").replace("M/s ", "").replace("MR. ", "").replace("               ", "").replace("TRADINGCO", "TRADING CO").trim() });
                    if ($scope.row.party != undefined) $rootScope.master.party_id = $scope.row.party.party_id;
                }
            }
            if (index == 1) {
                $f_rows = $($(tr.cells[0]).children()[0].rows[1].cells[0]).children()[0].rows;
                $scope.row.party.party_emd = $f_rows[0].cells[1].innerHTML;
                $scope.row.party.party_purchase_capacity = $f_rows[1].cells[1].innerHTML;
                $scope.row.party.party_bidding_capacity = $f_rows[2].cells[1].innerHTML;

                $party_lots = [];

                $l_rows = $($(tr.cells[0]).children()[0].rows[2].cells[0]).children()[1];
                for (let index = 1; index < $l_rows.rows.length; index++) {
                    if ($l_rows.rows[index].cells[1] != undefined) {
                        if ($($l_rows.rows[index].cells[0]).find("label").text().trim() != "") {
                            $party_lot = {};
                            $party_lot.party_lot_priority = $($l_rows.rows[index].cells[0]).find("label").text().trim();
                            var d = $($l_rows.rows[index].cells[1]).find("label").text().trim().replace("Lot No. ", "").trim().replace("S.B.", "").trim();
                            $party_lot.lotNo = d.split(" ")[0].split('/')[0];
                            $party_lot.lotSTANB = d.split(" ")[2];
                            $party_lot.party_lot_capt_id = $($l_rows.rows[index].cells[1]).find("label").text().trim().replace("Lot No. ", "").trim().replace("S.B.", "").trim();
                            $party_lot.party_lot_pur_rate = $($l_rows.rows[index].cells[2]).find("label").text().trim();
                            $party_lot.party_lot_qty_per_rate = $($l_rows.rows[index].cells[3]).find("label").text().trim();
                            if ($party_lot.party_lot_priority != "NIL") $party_lots.push($party_lot);
                        }
                    }
                }


                $scope.row.party_lot = $party_lots;


            }
            // $scope.mainTable = $(tr.cells[1]).children()[0];
        });
        $scope.view_table = false;
        $rootScope.show_toest("Data Loading");
    }

    $scope.btnContinewClick = async () => {
        $rootScope.show_toest("Saving.");
        $Party.set_party_lot({ "search": $rootScope.master, "party": $scope.row.party, "party_lot": $scope.row.party_lot }).then(x => {
            if (x == false) {
                $rootScope.show_toest("Kindly select party");
            } else {
                $rootScope.show_toest("Saved.");
                $scope.row = {};
                $scope.row.party = {};
                // location.reload();
                //$scope.tander.data = "";
                //window.open($rootScope.BaseUrl + "/#!/OpenRound" + "", "_self");
            }
        });
    }


    $scope.on_party_change = () => {
        var i = $scope.Party.find((o) => { return o.party_id === $rootScope.master.party_id });
        $scope.row.party.party_id = i.party_id;
    }

    $scope.on_round_change = async () => {
        $scope.Party = await $Party.get_party_by_round($rootScope.master);
        if ($scope.Party.length == 0) $rootScope.show_toest("No party find");
        else $rootScope.show_toest($scope.Party.length + " Party loaded");
    }
    if ($rootScope.master.party_id !== 0) $scope.on_round_change();

    // $scope.mainTable = "";
    // $scope.party_list = [];
    // $scope.view_table = true;
    // $scope.round = ['1 Ground','2 Ground','3 Ground','4 Ground'];
    // $scope.reset = () => {
    //     $scope.title = "";
    //     $rootScope.subtitle = "vehicle_data";
    //     $scope.vehicles = [];
    //     $scope.vehicle = {};
    //     $scope.search = {};
    //     $scope.action = 2;
    //     $scope.searchView = 1;


    //     if ($routeParams.vehicle != undefined) {
    //         $scope.action = 2;
    //         $Vehicle_data.get_by_id($routeParams.user_id).then(x => $scope.vehicle = x);
    //     } else if ($routeParams.vehicle_id == "") {
    //         $scope.vehicle = {};
    //     } else $Vehicle_data.load().then(x => $scope.vehicle = x);
    // }
    // $scope.add_vehicle = (ev) => {



    //     $rootScope.show_dialog('do you want to save this entry', "message", ev, () => {
    //         $scope.vehicles.push($scope.vehicle);
    //         $scope.vehicle = {};
    //         $scope.action = 1;
    //     });

    // }

    // $scope.edit_vehicle = (x) => {
    //     $scope.vehicle = x;
    //     $scope.action = 2;
    // }

    // $scope.openMenu = function ($mdMenu, ev) {
    //     originatorEv = ev;
    //     $mdMenu.open(ev);
    // };

    // $scope.add_new = () => {
    //     $scope.action = 2;
    //     $scope.vehicle = {};
    // }

    $scope.$on("onBack", () => history.back());
    $scope.$on("onAdd", () => $scope.action = 2);
    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);
    $scope.$on("onRefresh", () => $scope.tander = "");
});

app.controller('expectedResultCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Vehicle_ride) {


    $scope.reset = () => {
        $scope.title = "";
        $rootScope.subtitle = "vehicle_ride";
        $scope.action = 2;
        $scope.searchView = 1;


        if ($routeParams.vehicle != undefined) {
            $scope.action = 2;
            $Vehicle_ride.get_by_id($routeParams.user_id).then(x => $scope.vehicle = x);
        } else if ($routeParams.vehicle_id == "") {
            $scope.vehicle = {};
        } else $Vehicle_ride.load().then(x => $scope.vehicle = x);
    }


    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);

    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);
});

app.controller('resultCtrl', function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Vehicle_ride) {


    $scope.reset = () => {
        $scope.title = "";
        $rootScope.subtitle = "vehicle_ride";
        $scope.action = 2;
        $scope.searchView = 1;


        if ($routeParams.vehicle != undefined) {
            $scope.action = 2;
            $Vehicle_ride.get_by_id($routeParams.user_id).then(x => $scope.vehicle = x);
        } else if ($routeParams.vehicle_id == "") {
            $scope.vehicle = {};
        } else $Vehicle_ride.load().then(x => $scope.vehicle = x);
    }


    $scope.$on("onBack", () => {
        history.back();
    });
    $scope.$on("onAdd", () => $scope.action = 2);

    $scope.$on("onSearch", () => $scope.searchView = $scope.searchView == 1 ? 2 : 1);
});

app.controller("yearCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $FinancialYear) {
    $rootScope.title = "Year";
    $rootScope.subtitle = "Years";

    $scope.reset = () => {
        $rootScope.show_msg("Loading Years...");
        $scope.years = [];
        $scope.year = {};
        $FinancialYear.load().then(x => {
            $scope.years = x;
        });
    }
    $scope.reset();

    $scope.btnEdit = (x) => $scope.year = x;
    $scope.btnSaveClick = () => {
        $rootScope.show_msg("Saveing...");
        $FinancialYear.set($scope.year).then(x => $scope.reset());
    };


    $scope.btnrefresh = (x) => {
        $rootScope.show_msg("Refreing...");
        $FinancialYear.refresh({master : $scope.master, x : x}).then(x => {
            // $scope.years = x;
        }); 
    }
    

    $scope.$on("onRefresh", () => $scope.reset());
    $scope.$on("onAdd", () => $scope.reset());
});

app.controller("roundCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Round) {
    $rootScope.subtitle = "Rounds";

    $scope.reset = () => {
        $rootScope.show_msg("Loading Rounds...");
        $scope.rounds = [];
        $scope.round = {};
        $Round.load().then(x => {
            $scope.rounds = x;
        });
    }
    $scope.reset();

    $scope.btnEdit = (x) => $scope.round = x;
    $scope.btnSaveClick = () => {
        $rootScope.show_msg("Saveing...");
        $Round.set($scope.round).then(x => $scope.reset());
    };
    $scope.$on("onRefresh", () => $scope.reset());
    $scope.$on("onAdd", () => $scope.reset());
});

app.controller("partywiseCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Party) {
    $scope.reset = () => {
        $rootScope.subtitle = "Partywise Report";

        $scope.type = $routeParams.RptType != undefined ? $routeParams.RptType : "entry";
        $scope.list = [];
        $scope.search = {};
        $scope.Partys = [];
        $rootScope.show_msg("Loding...");

        if ($scope.type == "forcast") $Party.load_focast($rootScope.master).then(x => $scope.Partys = x);
        else $Party.load($rootScope.master).then(x => $scope.Partys = x);
        $rootScope.show_msg("Loaded");

    }
    $scope.hideFilter = function (item) {
        return item.party_lot_data.filter(e => e.party_lot_result_status).length > 0;
    }

    $scope.cheklot = x => {
        $out = false;
        for (let index = 0; index < x.party_lot_data.length; index++) {
            if (x.party_lot_data[index].party_lot_status == 'ALLOTED' || x.party_lot_data[index].party_lot_status == 'Luckey Rate')
                $out = true;
        }
        return $out;
    }

    // $scope.reset();


    $scope.btnLoadClick = () => {

        if ($routeParams.RptType == 'forcast') {
            $scope.type='forcast';
            $Party.load_focast($rootScope.master).then(x => $scope.list = x);
            return;
        } else if ($routeParams.RptType == 'Reports') {
            $Party.loadpartywise_reslut($rootScope.master).then(x => {
                // for (let index = 0; index < x.length; index++) {
                //     x[index].get_total = () => {
                //         var i = 0;
                //         if (x[index].party_lot_data.filter(e => e.party_lot_result_status).length > 0) {
                //             for (let ind = 0; ind < x[ind].party_lot_data.filter(e => e.party_lot_result_status).length; ind++) {
                //                 i += x[ind].party_lot_data[ind].Lot.lotSTANB;
                //             }
                //         }
                //         return i;
                //         // console.log(x[index].party_lot_data.length);
                //     };

                // }
                $scope.type = "Reports";
                $scope.list = x
            });
        } else if ($scope.type == "forcast") {
            $Party.load_focast($rootScope.master).then(x => $scope.Partys = x);
        } else{
            $scope.type = "entry";
            // $Party.load($rootScope.master).then(x => $scope.list = x);
            $Party.loadpartywise($rootScope.master).then(x => $scope.list = x);
        } 
         // for (let index = 0; index < x.length; index++) {
                //     x[index].get_total = () => {
                //         var i = 0;
                //         if (x[index].party_lot_data.filter(e => e.party_lot_result_status).length > 0) {
                //             for (let ind = 0; ind < x[ind].party_lot_data.filter(e => e.party_lot_result_status).length; ind++) {
                //                 i += x[ind].party_lot_data[ind].Lot.lotSTANB;
                //             }
                //         }
                //         return i;
                //         // console.log(x[index].party_lot_data.length);
                //     };

                // }

    }
    $scope.btnLoadClick();

    $scope.get_total_std = (x) => {
        var i = 0;
        if (x.party_lot_data.filter(e => e.party_lot_result_status).length > 0) {
            for (let ind = 0; ind < x.party_lot_data.filter(e => e.party_lot_result_status).length; ind++) {
                i += parseInt(x.party_lot_data[ind].Lot.lotSTANB);
            }
        }
        return i;

    }
    $scope.get_total = (x) => {
        var i = 0;
        if (x.party_lot_data.filter(e => e.party_lot_result_status).length > 0) {
            for (let ind = 0; ind < x.party_lot_data.filter(e => e.party_lot_result_status).length; ind++) {
                i += parseInt(x.party_lot_data[ind].party_lot_qty_per_rate);
            }
        }
        return i;

    }

    $scope.myFilter = function (item) {
        var out = false;
        if (!($scope.search.party_id == undefined || $scope.search.party_id == "")) {
            // return ($scope.search.party_id.filter(e => e === item.party_id).length > 0)
            return true;
        } else return true;
    };

    $scope.myFilterHeadCode = (item) => {
        var out = false;
        if($scope.search !== undefined){
            if (!($scope.search.party_head_code === undefined || $scope.search.party_head_code == "")) {
                return (parseInt($scope.search.party_head_code) == parseInt(item.party_head_code))
            } else return true;
        }else return true;
        
    }

    $scope.get_std_total = (x) => {
        var out = 0;
        x.party_lot_data.forEach(x => {
            if (x.party_lot_status == 'ALLOTED' || x.party_lot_status == 'Luckey Rate') out += parseInt(x.Lot.lotSTANB)
        });
        return out;
    }
    $scope.get_amt_total = (x) => {
        var out = 0;
        // console.log(x.party_lot_data);
        x.party_lot_data.forEach(x => {
            if (x.party_lot_status == 'ALLOTED' || x.party_lot_status == 'Luckey Rate') out += parseInt(x.party_lot_qty_per_rate)
        });
        return out;
    }

    $scope.get_sbag_tott = (x) => {
        var out = 0;
        x.party_lot_data.forEach(y => {
            out += parseInt(y.Lot.lotSTANB)
        });
        return out;
    }
    $scope.get_amt_tott = (x) => {
        var ht = 0;
        x.party_lot_data.forEach(y => {
            ht += parseInt(y.party_lot_qty_per_rate)
        });
        return ht;
    }
    $scope.$on("onClear", () => $scope.reset());
    $scope.$on("onDownload", () => {
        var q = $rootScope.master;
        q.file_type = "csv";
        window.open(BaseUrl + "Apis/Party/loadpartywise?file_type=csv&financial_year_id=" + $rootScope.master.financial_year_id + "&round_id=" + $rootScope.master.round_id + "&state_id=" + $rootScope.master.state_id);
        $rootScope.show_toest("Downloading");
    });
});

app.controller("lotwiseCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast) { });

app.controller("lotwiseReportCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party) {

    $rootScope.subtitle = "Lot Wise Report";
    $scope.lots = [];
    $scope.lot_master = [];
    $scope.lotDISTU = [];
    $scope.plist = [];
    $scope.search = {};
    // $scope.party_display_name = {}s;

    $Party.load($rootScope.master).then(x => $scope.plist = x);

    $scope.reset = () => {
        $rootScope.show_msg("Loading...");
        $routeParams.RptType != undefined ? $routeParams.RptType : "entry";
        $scope.search.uptorank = 200;
        $Lot.load($rootScope.master).then(x => {
            $scope.lot_master = x;
            $rootScope.show_msg("Loaded");
            $scope.lotDISTU = mygroupby(x, 'lotDISTU');
        });
        $Lot.search($rootScope.master).then(this_lots => {
            $scope.lots = this_lots;
            // this_lots.forEach(x => {
            //         if (x.parties.length > 0) templist.push({ "lotName": x.lotName, "lotDISTU": x.lotDISTU });
            // });
            // $scope.lots = templist;
            // console.log($scope.lots);
        })
    }
    $scope.reset();

    $scope.get_party_name = () => {
        return "";
    }

    $scope.myFilter = function (item) {
        var out = false;
        if (!($scope.search.lotId == undefined || $scope.search.lotId == "")) {
            return ($scope.search.lotId.filter(e => e === item.lotNo).length > 0);
        }
        if (!($scope.search.party_head_code == undefined || $scope.search.party_head_code == "")) {
            $ret = false;
            // for (let index = 0; index < item.party_lot_data.length; index++)
            //     if (parseInt(item.party_lot_data[index].party.party_head_code) == parseInt($scope.search.party_head_code)) $ret = true;
            // return $ret;
            for (let index = 0; index < item.party_lot_data.length; index++)
                if (parseInt(item.party_lot_data[index].party.party_head_code) == parseInt($scope.search.party_head_code)) {
                    // item.party_lot_data[index].party.party_display_name == $scope.party_display_name;
                    // console.log($scope.party_display_name);
                    $ret = true;
                }
            return $ret;
        }
        return true;

    };

    $scope.myValueFunction = function (x) {
        return parent(x.party_lot_pur_rate)
        // return card.values.opt1 + card.values.opt2;
    }

    $scope.lotDISTUFilter = function (item) {
        var out = false;
        if (!($scope.search.lotDISTU == undefined || $scope.search.lotDISTU == "")) {
            return ($scope.search.lotDISTU.filter(e => e === item.lotDISTU).length > 0)
        } else return true;
    };

    $scope.leasethen = (item) => parseInt(item.party_lot_rank) < parseInt($scope.search.uptorank)

    $scope.btnLoadClick = () => $scope.reset();

    mygroupby = (array, key) => {
        const $out = [];
        $.each(array, (i, x) => {
            if ($.inArray(x[key], $out) == -1) $out.push(x[key]);
        });
        return $out;
    }

    $scope.get_display_name = (x) => {
        for (let i = 0; i < $scope.plist.length; i++) {
            const element = $scope.plist[i];
            if (element.party_head_code == x) return element.party_name;
        }
        return "--";

    }


});

app.controller("capacityCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $PartyGroup, $Party) {
    $scope.list = [];
    $scope.list_group = [];
    $scope.total = { emd: 0, cap: 0, tander: 0 };
    $rootScope.subtitle = "Capacity";
    $scope.btnLoadClick = () => {
        $rootScope.show_toest("Loading");
        $PartyGroup.get_partyGroup($rootScope.master).then(xx => {
            list_group = xx;
            $Party.capacity($rootScope.master).then(x => {
                $scope.list = x;

                $scope.list.forEach(x => {
                    $scope.total.emd += parseInt(x.party_emd);
                    $scope.total.cap += x.party_purchase_capacity == null ? 0 : parseInt(x.party_purchase_capacity);
                    $scope.total.tander += parseInt(x.party_lot_data.length);
                });
                if ($scope.list.length == 0) $rootScope.show_toest("No data found");
                else $rootScope.show_toest("Loaded");
            });
        });
    }
    $scope.btnLoadClick();

    $scope.$on("onDownload", () => {
        var q = $rootScope.master;
        q.file_type = "csv";
        window.open(BaseUrl + "Apis/Party/capacity?file_type=csv&financial_year_id=" + $rootScope.master.financial_year_id + "&round_id=" + $rootScope.master.round_id + "&state_id=" + $rootScope.master.state_id);
        $rootScope.show_toest("Downloading");
    });

    $scope.delete_all_loats = (ev, x) => {
        var confirm = $mdDialog.confirm()
            .title('Delete all lots')
            .textContent("Are you sure you want to delete all lots?")
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('yes')
            .cancel('no');

        $mdDialog.show(confirm).then(() => {
            $rootScope.show_msg("Loading...");
            $Party.delete_all_loats({ root: $rootScope.master, x: x }).then(x => {
                $rootScope.show_msg("Deleted");
                $scope.btnLoadClick();
            }).catch(x => {
                $rootScope.show_msg("Error");
            }).finally(() => {
                $rootScope.show_msg("Loaded");
            });
            // $mdToast.show($mdToast.simple().textContent('You Cancel').hideDelay(3000));
        });


    };

    $scope.get_group_name = (x) => {
        $out = "";
        list_group.forEach(y => {
            // console.log(y);
            y.parties.forEach(z => {
                if (z.party_id == x.party_id) $out = `(${y.party_group_name})`;
            });
        });
        return $out;
        // console.log(x);
    }
});

app.controller("forcastCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Party) {

    $rootScope.subtitle = "forcast";
    $scope.btnAllocateRankClick = () => {
        $Party.allocateRank($rootScope.master).then(x => {
            console.log(x);
            $rootScope.show_msg("Allocating Rank...");
        });
    };
    $scope.btnAllocateLotClick = () => {
        $rootScope.loading = true;
        $rootScope.show_msg("Allocating Lots...");
        $Party.allocateLot($rootScope.master).then(x => {
            $rootScope.loading = false;
            $rootScope.show_msg("Allocating Lots...");
        });
    };

    $scope.btnRemoveAllocateLotClick = () => {
        $Party.removeallocateLot($rootScope.master).then(x => {
            $rootScope.loading = false;
            $rootScope.show_msg("Removing Allocat Lots...");
        });
    }

    // $scope.list = [];
    // $scope.btnLoadClick = () => $Party.capacity($rootScope.master).then(x => $scope.list = x);
    // $scope.btnLoadClick();
});

app.controller("lotCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot) {
    $rootScope.subtitle = "Lot";
    $scope.type = $routeParams.RptType != undefined ? $routeParams.RptType : "entry";
    $scope.lots = [];
    $rootScope.show_toest("Loading");

    if ($scope.type == 'BALANCE') {
        $Lot.balance_lot($rootScope.master).then(x => {
            var list = [];
            var name = "";
            var i = 0;
            var sb = 0;
            for (let inde = 0; inde < x.length; inde++) {
                if (name == "") name = x[inde].lotDISTU;
                x[inde].action = false;
                if (name != x[inde].lotDISTU) {
                    name = x[inde].lotDISTU;
                    // sb += parseInt(x[inde].lotSTANB);
                    // x.find(d => d.lotDISTU == x[inde].lotDISTU).forEach(element => {
                    //     sb += element.lotSTANB;
                    // });
                    list.push({ lotACTB: x[inde - 1].lotDISTU, lotSTANB: sb });
                    sb = 0;
                    sb += parseInt(x[inde].lotSTANB);
                    i++;
                    x[inde].ind = i;
                    // console.log(x.find(d => d.lotDISTU == y.lotDISTU));
                } else {
                    i++;
                    x[inde].ind = i;
                    sb += parseInt(x[inde].lotSTANB);
                }
                list.push(x[inde]);
            }

            // x.forEach(y => {
            //     y.ind = i; 
            //     i++;
            //     if(name == "") name = y.lotDISTU;
            //     y.action = false;
            //     if(name != y.lotDISTU){
            //         name = y.lotDISTU;
            //         var sb = 0;
            //         x.find(d => d.lotDISTU == y.lotDISTU).forEach(element => {
            //             sb += element.lotSTANB;
            //         });
            //         list.push({lotACTB : y.lotDISTU, lotSTANB : sb});
            //         // console.log(x.find(d => d.lotDISTU == y.lotDISTU));
            //     } 
            //     list.push(y);
            // });
            $scope.lots = list;
        });

    } else $Lot.load($rootScope.master).then(x => {
        x.forEach(y => {
            y.action = false;
        });
        var list = [];
        var name = "";
        var i = 0;
        var sb = 0;
        for (let inde = 0; inde < x.length; inde++) {
            if (name == "") name = x[inde].lotDISTU;
            x[inde].action = false;
            if (name != x[inde].lotDISTU) {
                name = x[inde].lotDISTU;
                // sb += parseInt(x[inde].lotSTANB);
                // x.find(d => d.lotDISTU == x[inde].lotDISTU).forEach(element => {
                //     sb += element.lotSTANB;
                // });
                list.push({ lotACTB: x[inde - 1].lotDISTU, lotSTANB: sb });
                sb = 0;
                sb += parseInt(x[inde].lotSTANB);
                i++;
                x[inde].ind = i;
                // console.log(x.find(d => d.lotDISTU == y.lotDISTU));
            } else {
                i++;
                x[inde].ind = i;
                sb += parseInt(x[inde].lotSTANB);
            }
            list.push(x[inde]);
        }

        // for (let index = 0; index < x.length; index++) {
        //     var num = '';
        //     var str = '';

        //     num += $.isNumeric(x[index].lotNo.charAt(0)) ? x[index].lotNo.charAt(0) : '';
        //     num += $.isNumeric(x[index].lotNo.charAt(1)) ? x[index].lotNo.charAt(1) : '';
        //     num += $.isNumeric(x[index].lotNo.charAt(2)) ? x[index].lotNo.charAt(2) : '';
        //     num += $.isNumeric(x[index].lotNo.charAt(3)) ? x[index].lotNo.charAt(3) : '';
        //     num += $.isNumeric(x[index].lotNo.charAt(4)) ? x[index].lotNo.charAt(4) : '';
        //     num = num.trim()

        //     str += $.isNumeric(x[index].lotNo.charAt(0)) ? '' : x[index].lotNo.charAt(0);
        //     str += $.isNumeric(x[index].lotNo.charAt(1)) ? '' : x[index].lotNo.charAt(1);
        //     str += $.isNumeric(x[index].lotNo.charAt(2)) ? '' : x[index].lotNo.charAt(2);
        //     str += $.isNumeric(x[index].lotNo.charAt(3)) ? '' : x[index].lotNo.charAt(3);
        //     str += $.isNumeric(x[index].lotNo.charAt(4)) ? '' : x[index].lotNo.charAt(4);
        //     str.trim();

        //     x[index].lotInd = num;
        //     x[index].lotTxt = str;
        //             x[inde].ind = index;    
        // }
        $scope.lots = list;
    });

    $scope.btnLoadClick = () => {
        $rootScope.show_toest("Loading");
        $Lot.load($rootScope.master).then(x => {
            $rootScope.show_toest("Loaded");
            $scope.lots = x;
        });
    };
    $scope.btnSaveClick = () => {
        $rootScope.show_toest("Saving");
        $Lot.add_lots($rootScope.master).then(x => {
            $rootScope.show_toest("Saved");
        });
        // $Lot.load($rootScope.master).then(x => $scope.lots = x);
    };
    $scope.btnindexClick = async () => {
        $Lot.set_index($rootScope.master).then(x => {
            $rootScope.show_toest("Indexing");
        });
        // for (let index = 0; index < $scope.lots.length; index++)
        //     await $Lot.set($scope.lots[index]).then(x => $rootScope.show_toest("Indexing"));



        // $rootScope.show_toest("Indexing");

        // $Lot.load($rootScope.master).then(x => {
        //     $rootScope.show_toest("Loaded");
        //     $scope.lots = x;
        // });

        // $rootScope.show_toest("Indexing completed");
        // console.log($scope.lots);
        // $scope.lots.forEach(x => {
        //     x.lotInd = parseInt(x.lotNo);
        // });
    };

    $scope.roundd_set = async () => {
        console.log($scope.lots);
    };

    $scope.btnSaveRoundClick = (x) => {
        x.action = false;
        $Lot.set(x).then(()=>{
            $rootScope.show_toest("Saved");
        });
    }

    $scope.round_change = () => {
        $scope.lots = [];

        $Lot.load($rootScope.master).then(x => $scope.lots = x);
    }
    // $Lot.Lot($rootScope.master).then(x => {
    //     $scope.list = x;
    //     $scope.list.forEach(x => {
    //         $scope.total.stb += parseInt(x.lotSTANB);
    //     });
    //     if ($scope.list.length == 0) $rootScope.show_toest("No data found");
    //     else $rootScope.show_toest("Loaded");
    // });
    $scope.get_sbag = () => {
        var out = 0;
        $scope.lots.forEach(y => {
            if (y.ind) out += parseInt(y.lotSTANB)
        });
        return out;
    }
});

app.controller("partygroupCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party, $PartyGroup) {
    $rootScope.subtitle = "Party Group";
    $scope.party_group = {};
    $scope.Partys = [];
    $scope.party = {};
    $scope.party_group_flag = false;
    $scope.reset = () => {
        $scope.party_groups = [];
        $scope.Partys = [];
        $scope.party = {};
        $scope.party_group = {};
        $rootScope.show_msg("Loading...");
        $Party.load($rootScope.master).then(x => {
            $scope.Partys = x;
        });
        $PartyGroup.get_partyGroup($rootScope.master).then(x => {
            $scope.party_groups = x;
        });
    }
    $scope.reset();
    $scope.btnEdit = (x) => {
        $scope.party_group = x;
        $scope.party_group_flag = true;
        $rootScope.show_msg("Kindly edit " + $scope.party_group.party_group_name);
    };

    $scope.btnSaveListClick = () => {
        $PartyGroup.set_partyGroup({ "new": $scope.new, "year": $scope.party_group });
    }

    $scope.btnSaveClick = () => {
        $rootScope.show_msg("Saveing...");
        $scope.party_group.financialYearFinancialYearId = $rootScope.master.financial_year_id;
        $scope.party_group.stateStateId = $rootScope.master.state_id;
        $scope.party_group.roundRoundId = $rootScope.master.round_id;
        if ($scope.party_group.party_group_id != undefined) {
            $PartyGroup.update($scope.party_group).then(x => $scope.reset());
            $scope.party_group_flag = false;
        } else {
            console.log($scope.party_group);
            $PartyGroup.add_partyGroup($scope.party_group).then(x => $scope.reset());
            $scope.party_group_flag = false;
        }
    }

    $scope.btnList = (x) => {
        $scope.party_group = x;
        $scope.party_group_flag = true;
        $rootScope.show_msg("List Loaded");
    }

    $scope.btnAddClick = (x) => {
        $PartyGroup.add_party_to_group({ group: $scope.party_group, party: $scope.party }).then(x => {
            $scope.party_group = x;
        });
    }

    $scope.btnDelete = (x) => {
        $PartyGroup.remove_party_to_group({ group: $scope.party_group, party: x }).then(x => {
            $scope.party_group = x;
        });
    }

    $scope.party_change = (x) => {
        $scope.party = x;
    }

    $scope.party_head_change = () => {
        $scope.Partys.forEach(x => {
            if (x.party_head_code == $scope.party.party_head_code) {
                $scope.party = x;
            }
        });
    }
});

app.controller("groupWiseReportCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $PartyGroup) {
    $rootScope.subtitle = "Group Wise Report";
    $scope.reset = () => {
        $scope.type = $routeParams.RptType != undefined ? $routeParams.RptType : "entry";
        $scope.search = {};
        $scope.PartyGroup = [];
        if ($scope.type == "result") {
            $rootScope.subtitle = "Group Wise Report";
            $PartyGroup.get_partyGroup_rpt_result($rootScope.master).then(x => {
                $scope.PartyGroup = x;
            });
        } else {
            $PartyGroup.get_partyGroup_rpt($rootScope.master).then(x => {
                $scope.PartyGroup = x;
            });
        }
    }
    $scope.reset();

    $scope.sorterFunc = function (person) {
        return parseInt(person.party_lot_rank);
    };



    $scope.myFilter = function (item) {
        var out = false;
        if (!($scope.search.party_group_id == undefined || $scope.search.party_group_id == "")) {
            return ($scope.search.party_group_id.filter(e => e === item.party_group_id).length > 0)
        } else return true;
    };

    $scope.get_std_total = (x) => {
        var out = 0;
        for (const xi of x.party_lot_data)  out += parseInt(xi.Lot.lotSTANB.toString())
        return out;
    }
    $scope.get_amt_total = (x) => {
        var out = 0;
        for (const xi of x.party_lot_data) out += parseInt(xi.party_lot_qty_per_rate.toString()) 
        // console.log(x.party_lot_data);
        // x.party_lot_data.forEach(x => {
        //     if (x.party_lot_status == 'ALLOTED' || x.party_lot_status == 'Luckey Rate') out += parseInt(x.party_lot_qty_per_rate)
        // });
        return out;
    }

    $scope.get_lots = (x) => {
        var i = 0;
        for (const y of x.parties)  for (const z of y.party_lot_data) i++
        // x.parties.forEach(y => {
        //     y.party_lot_data.forEach(z => {
        //         if (z.party_lot_status == 'ALLOTED' || z.party_lot_status == 'Luckey Rate') i++;
        //     });
        // });
        return i;
    }

    $scope.get_capacity = (x) => {
        var i = 0;
        x.parties.forEach(y => {
            i += parseInt(y.party_purchase_capacity);
            y.party_lot_data.forEach(z => {
                // i += parseInt(y.party_bidding_capacity);
                // if(z.party_lot_status=='ALLOTED' || z.party_lot_status=='Luckey Rate') i++;
            });
        });

        return i;
    }

    $scope.get_bags = (x) => {
        var i = 0;
        for (const y of x.parties)  for (const z of y.party_lot_data) i += parseInt(z.Lot.lotSTANB)
        return i;
    }



    $scope.get_royalty = (x) => {
        var i = 0;
        for (const y of x.parties)  for (const z of y.party_lot_data) i += parseInt(z.party_lot_qty_per_rate);
        // x.parties.forEach(y => {
        //     y.party_lot_data.forEach(z => {
        //         if (z.party_lot_status == 'ALLOTED' || z.party_lot_status == 'Luckey Rate') i += parseInt(z.party_lot_qty_per_rate)
        //     });
        // });
        return i;
    }



});

app.controller("partywisesummryreportsCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party) {
    $rootScope.subtitle = "Party Wise Summry Reports";
    $scope.list = [];
    $scope.row = {};
    $scope.row.party_purchase_capacity = 0;
    $scope.row.totLot = 0;
    $scope.row.totbag = 0;
    $scope.row.totamnt = 0;
    $scope.row.main = 0;

    $Party.get_party_wise_summery($rootScope.master).then(x => {
        $scope.list = x;

        for (let i = 0; i < $scope.list.length; i++) {
            const element = $scope.list[i];
            // console.log(element);
            $scope.row.party_purchase_capacity += parseInt(element.party.party_purchase_capacity);
            // console.log($scope.row.party_purchase_capacity);
            $scope.row.totLot += parseInt(element.lott_no);
            $scope.row.totbag += parseInt(element.bags);
            $scope.row.totamnt += parseInt(element.party_lot_qty_per_rate);
            $scope.row.main += parseInt(element.main);
        }
    });
});

app.controller("DivisionWiseSummaryReportsCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party) {
    $rootScope.subtitle = "Division Wise Summary Reports";
    $scope.list = [];
    $scope.row = { rate: 0, lotss: 0 };
    $scope.reset = () => {
        $rootScope.show_msg("Loading...");
        $Lot.DivisionWiseSummaryReports($rootScope.master).then(x => {
            $scope.list = x;
            x.forEach(y => {
                $scope.row.rate += parseInt(y.rate);
                $scope.row.lotss += parseInt(y.lots);
            });
        });
        $rootScope.show_msg("Loaded");
    }
    $scope.reset();

});

app.controller("LotReportWithPartyCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party) {
    $rootScope.subtitle = "Lot Report With Party";
    $scope.lots = [];
    $scope.reset = () => {
        $rootScope.show_msg("Loading...");
        $Lot.LotReportWithParty($rootScope.master).then(x => {
            $scope.lots = x;
        });
        $rootScope.show_msg("Loaded");
    }
    $scope.reset();
    $scope.get_sbag = () => {
        var out = 0;
        $scope.lots.forEach(y => {
            out += parseInt(y.lotSTANB)
        });
        return out;
    }

});


app.controller("lotwiseSinglePartyReportCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $PartyGroup) {
    $rootScope.subtitle = "Lot wise Single Party Report";
    $scope.reset = () => {
        $scope.PartyGroup = [];
        $PartyGroup.get_partyGroup_rpt($rootScope.master).then(x => {
            $scope.PartyGroup = x;
        });
    }
    $scope.reset();
});

app.controller("groupWiseSummaryReportCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $PartyGroup) {
    $rootScope.subtitle = "Group Wise Summary Report";
    $scope.reset = () => {
        $scope.type = $routeParams.RptType != undefined ? $routeParams.RptType : "entry";
        $scope.PartyGroup = [];
        $scope.total = {};
        $scope.total.STD = 0;
        $scope.total.capacity  = 0;
        $scope.total.Amount = 0;
        $scope.total.AVRG = 0;
        if ($scope.type == "result") {
            $PartyGroup.get_partyGroup_rpt_result($rootScope.master).then(x => {
                $scope.PartyGroup = x;
                for (let index = 0; index < $scope.PartyGroup.length; index++) {
                    const element = $scope.PartyGroup[index];
                    $scope.total.STD += $scope.get_bags(element);
                    $scope.total.capacity += $scope.get_capacity(element);
                    $scope.total.Amount += $scope.get_royalty(element);
                    $scope.total.AVRG += parseInt(($scope.get_royalty(element) / $scope.get_bags(element)));
                }

                $scope.total.AVRG = ($scope.total.Amount / $scope.total.STD).toFixed(0);

                // x.forEach(y => {
                //     $scope.total.STD += $scope.get_bags(y);
                //     $scope.total.Amount += $scope.get_royalty(y);
                //     $scope.total.AVRG += parseInt(($scope.get_royalty(y) / $scope.get_bags(y)));
                //     // (parseInt($scope.get_royalty(y)) / parseInt($scope.get_bags(y))).toFixed(0);
                // });
            });

        } else
            $PartyGroup.get_partyGroup_rpt($rootScope.master).then(x => {
                $scope.PartyGroup = x;
                for (let index = 0; index < $scope.PartyGroup.length; index++) {
                    const element = $scope.PartyGroup[index];
                    $scope.total.STD += $scope.get_bags(element);
                    $scope.total.capacity += $scope.get_capacity(element);
                    $scope.total.Amount += $scope.get_royalty(element);
                    $scope.total.AVRG += parseInt(($scope.get_royalty(element) / $scope.get_bags(element)));
                }

                $scope.total.AVRG = ($scope.total.Amount / $scope.total.STD).toFixed(0);

                // x.forEach(y => {
                //     $scope.total.STD += $scope.get_bags(y);
                //     $scope.total.Amount += $scope.get_royalty(y);
                //     $scope.total.AVRG += parseInt(($scope.get_royalty(y) / $scope.get_bags(y)));
                //     // (parseInt($scope.get_royalty(y)) / parseInt($scope.get_bags(y))).toFixed(0);
                // });
            });
    }
    $scope.reset();

    // $scope.get_capacity = (x) => {
    //     var i = 0;
    //     x.parties.forEach(y => {
    //         i += parseInt(y.party_bidding_capacity);
    //         y.party_lot_data.forEach(z => {
    //             // i += parseInt(y.party_bidding_capacity);
    //             // if(z.party_lot_status=='ALLOTED' || z.party_lot_status=='Luckey Rate') i++;
    //         });
    //     });
    //     return i;
    // }

    $scope.get_bags = (x) => {
        var i = 0;
        x.parties.forEach(y => {
            y.party_lot_data.forEach(z => {
                if (z.party_lot_status == 'ALLOTED' || z.party_lot_status == 'Luckey Rate') i += parseInt(z.Lot.lotSTANB)
            });
        });

        return i;
    }

    $scope.get_capacity = (x) =>{
        var i = 0;
        x.parties.forEach(y => {
            i += parseInt(y.party_purchase_capacity);
            // y.party_lot_data.forEach(z => {
            //     // i += parseInt(y.party_bidding_capacity);
            //     // if(z.party_lot_status=='ALLOTED' || z.party_lot_status=='Luckey Rate') i++;
            // });
        });
        return parseFloat(i);
    }

    $scope.get_bags_result = (x) => {
        var i = 0;
        x.parties.forEach(y => {
            y.party_lot_data.forEach(z => {
                // if (z.party_lot_status == 'ALLOTED' || z.party_lot_status == 'Luckey Rate') i += parseInt(z.Lot.lotSTANB)
                if (z.party_lot_result_status == true) i += parseInt(z.Lot.lotSTANB)
            });
        });

        return i;
    }

    $scope.get_royalty = (x) => {
        var i = 0;
        x.parties.forEach(y => {
            y.party_lot_data.forEach(z => {
                if (z.party_lot_status == 'ALLOTED' || z.party_lot_status == 'Luckey Rate') i += parseInt(z.party_lot_qty_per_rate)
            });
        });

        return i;
    }

    $scope.get_royalty_result = (x) => {
        var i = 0;
        x.parties.forEach(y => {
            y.party_lot_data.forEach(z => {
                // if (z.party_lot_status == 'ALLOTED' || z.party_lot_status == 'Luckey Rate') i += parseInt(z.party_lot_qty_per_rate)
                if (z.party_lot_result_status == true) i += parseInt(z.party_lot_qty_per_rate)
            });
        });

        return i;
    }

    $scope.get_lots = (x) => {
        var i = 0;
        if (x == undefined) return i;
        // console.log(x);
        // console.log("---------------------");

        for (let index = 0; index < x.parties.length; index++) {
            // for (let ii = 0; ii < x[index].parties.length; ii++) {
            for (let ind = 0; ind < x.parties[index].party_lot_data.length; ind++) {
                // const element = array[ind];
                if (x.parties[index].party_lot_data[ind].party_lot_status == 'ALLOTED' || x.parties[index].party_lot_data[ind].party_lot_status == 'Luckey Rate') i++;

            }
            // i += x.parties[index].party_lot_data.length;
            // }
        }
        return i;
    }

    $scope.get_lots_reslut = (x) => {
        var i = 0;
        if (x == undefined) return i;
        // console.log(x);
        // console.log("---------------------");

        for (let index = 0; index < x.parties.length; index++) {
            // for (let ii = 0; ii < x[index].parties.length; ii++) {
            for (let ind = 0; ind < x.parties[index].party_lot_data.length; ind++) {
                // const element = array[ind];
                if (x.parties[index].party_lot_data[ind].party_lot_result_status == true) i++;

            }
            // i += x.parties[index].party_lot_data.length;
            // }
        }
        return i;
    }

    $scope.get_lots_total = () => {
        var i = 0;
        for (let index = 0; index < $scope.PartyGroup.length; index++) {
            i += $scope.get_lots($scope.PartyGroup[index])
        }
        return i;
    }
});

app.controller("resultEntryCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party) {
    $rootScope.subtitle = "Result Entry";
    $scope.reset = () => {
        $rootScope.show_msg("Loading...");

        $scope.list = [];
        $scope.search = {};
        $scope.Partys = [];
        $Party.load($rootScope.master).then(x => {
            $scope.Partys = x;
            $rootScope.show_msg("Loaded");
        });
    }

    $scope.reset();


    $scope.on_result_change = (x) => {
        $Party.update_party_lot_data(x).then(x => {

        })
    }


    $scope.btnLoadClick = () => {
        $Party.loadpartywise($rootScope.master).then(x => $scope.list = x);
    }
    $scope.btnLoadClick();

    $scope.myFilter = function (item) {
        var out = false;
        if (!($scope.search.party_id == undefined || $scope.search.party_id == "")) {
            return ($scope.search.party_id.filter(e => e === item.party_id).length > 0)
        } else return true;
    };

    $scope.$on("onClear", () => $scope.reset());
});

app.controller("farmCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $farm) {
    $rootScope.subtitle = "Farm";
    $scope.reset = () => {
        $scope.list = [];
        $scope.farm = {};
        $scope.Partys = [];
        $farm.load($rootScope.master).then(x => $scope.farm = x.length == 0 ? {} : x[0]);
    }

    $scope.reset();
    $scope.txtChange = () => $farm.set($scope.farm).then(x => $scope.reset());

    // $scope.on_result_change = (x) =>{
    //     $Party.update_party_lot_data(x).then(x =>{

    //     })
    // }


    // $scope.btnLoadClick = () => {
    //     $Party.loadpartywise($rootScope.master).then(x => $scope.list = x);
    // }
    // $scope.btnLoadClick();

    // $scope.myFilter = function (item) {
    //     var out = false;
    //     if (!($scope.search.party_id == undefined || $scope.search.party_id == "")) {
    //         return ($scope.search.party_id.filter(e => e === item.party_id).length > 0)
    //     }
    //     else return true;
    // };

    $scope.$on("onClear", () => $scope.reset());
});

app.controller("dashbordCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $farm, $User) {
    $scope.set_data = (round_id, state_id) => {
        $rootScope.master.round_id = round_id;
        $rootScope.master.state_id = state_id;
        $User.set_session($rootScope.master);
    }

    $scope.set_tander = (round_id, state_id) => {
        $rootScope.master.round_id = round_id;
        $rootScope.master.state_id = state_id;
        $User.set_session($rootScope.master);
        // $rootScope.load_url("Tander");
        window.open($rootScope.BaseUrl + "/#!/" + "Tander", "_self");
    }

    $scope.go_round = (round_id, state_id) => {
        $rootScope.master.round_id = round_id;
        $rootScope.master.state_id = state_id;
        $User.set_session($rootScope.master);
        // $rootScope.load_url("Tander");
        window.open($rootScope.BaseUrl + "/#!/" + "OpenRound", "_self");
    }

    // $rootScope.subtitle = "Farm";
    // $scope.reset = () => {
    //     $scope.list = [];
    //     $scope.farm = {};
    //     $scope.Partys = [];
    //     $farm.load($rootScope.master).then(x => $scope.farm = x.length == 0 ? {} : x[0]);
    // }

    // $scope.reset();
    // $scope.txtChange = () =>  $farm.set($scope.farm).then( x =>  $scope.reset());


    // $scope.$on("onClear", () => $scope.reset());
});

app.controller("partydisplaynameCtrl", function ($scope, $routeParams, $rootScope, $mdDialog, $mdToast, $Lot, $Party, $PartyGroup) {
    $scope.list = [];
    $scope.list_group = [];
    $scope.total = { emd: 0, cap: 0, tander: 0 };
    $rootScope.subtitle = "Party Dispaly Name";
    $scope.btnLoadClick = () => {
        $rootScope.show_toest("Loading");
        $PartyGroup.get_partyGroup($rootScope.master).then(xx => {
            list_group = xx;
            $Party.capacity($rootScope.master).then(x => {
                $scope.list = x;
                $scope.list.forEach(x => {
                    x.party_display_name_s = x.party_display_name
                    x.party_display_name = x.party_name;
                    $scope.total.emd += parseInt(x.party_emd);
                    $scope.total.cap += parseInt(x.party_purchase_capacity);
                    $scope.total.tander += parseInt(x.party_lot_data.length);
                });
                if ($scope.list.length == 0) $rootScope.show_toest("No data found");
                else $rootScope.show_toest("Loaded");
            });
        });
    }
    $scope.btnLoadClick();

    $scope.$on("onDownload", () => {
        var q = $rootScope.master;
        q.file_type = "csv";
        window.open(BaseUrl + "Apis/Party/capacity?file_type=csv&financial_year_id=" + $rootScope.master.financial_year_id + "&round_id=" + $rootScope.master.round_id + "&state_id=" + $rootScope.master.state_id);
        $rootScope.show_toest("Downloading");
    });

    $scope.get_group_name = (x) => {
        $out = "";
        list_group.forEach(y => {
            // console.log(y);
            y.parties.forEach(z => {
                if (z.party_id == x.party_id) $out = `(${y.party_group_name})`;
            });
        });
        return $out;
        // console.log(x);
    }
    $scope.save = async () => {
        $scope.list.forEach(async element => {
            element.party_lot_data = undefined;
            await $Party.set(element);
        });
        $rootScope.show_toest("Saved");
    }

});

app.controller("TanderdateCtrl", function ($scope, $rootScope, $roundD) {
    $scope.round = {};
    $scope.rounds = [];
    $rootScope.subtitle = "Tander Date";
    $roundD.load().then(x => {
        $scope.rounds = x;
    });
    $scope.save = () => {
        $roundD.set($scope.round).then(x => {
            $scope.round = {};
            $roundD.load().then(x => {
                $rootScope.show_toest("Saved");

                $scope.rounds = x;
            });
        });
    }
    $scope.Update = (x) => {
        $scope.round = x;
    }

    $scope.Delete = (x) => {
        $roundD.destoy(x).then(x => {
            $scope.round = {};
            $roundD.load().then(x => {
                $rootScope.show_toest("Deleted");

                $scope.rounds = x;
            });
        });
    }
});