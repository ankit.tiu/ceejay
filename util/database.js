const Sequelize = require("sequelize");

// var test = new Sequelize(
//     "database",
//     process.env.USER,
//     process.env.PASSWORD,
//     {
//       host: "0.0.0.0",
//       dialect: "sqlite",
//       pool: {
//         max: 5,
//         min: 0,
//         idle: 10000
//       },
//       // Data is stored in the file `database.sqlite` in the folder `db`.
//       // Note that if you leave your app public, this database file will be copied if
//       // someone forks your app. So don't use it to store sensitive information.
//       storage: "/sandbox/src/db/database.sqlite"
//     }
//   );




// const abcd = new Sequelize('sqlite::memory:', {
//     // Choose one of the logging options
//     // logging: console.log,                  // Default, displays the first parameter of the log function call
//     // logging: (...msg) => console.log(msg), // Displays all log function call parameters
//     // logging: false,                        // Disables logging
//     // logging: msg => logger.debug(msg),     // Use custom logger (e.g. Winston or Bunyan), displays the first parameter
//     // logging: logger.debug.bind(logger)     // Alternative way to use custom logger, displays all messages
//   });


// const sequelize = new Sequelize("ceejay_15", "root", "", {
//     dialect: "mysql",
//     pool: {
//         max: 5,
//         min: 0,
//         idle: 10000
//     },
//     host: "localhost",
//     logging: false
// });

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'system.dll',
    logging: false
});




module.exports = sequelize;