const Sequelize = require("sequelize");

const sequelize = require("../util/database");
const financial_year = require("./Financial_year");
const Round = require("./Round");
const State = require("./State");

const Lot = sequelize.define("Lot", {
    lotId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    lotNo: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotNameU: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotPlace: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotGodawn: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotACTB: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lotSTANB: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lotDISTU: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotOption: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotUpset: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lotLRATE: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lotWT: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lotROUNDPASS: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lotFlag: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    lotInd: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    lotTxt: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    stateStateId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    roundRoundId: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    financialYearFinancialYearId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
}, { paranoid: true });

Lot.belongsTo(financial_year);
Lot.belongsTo(State);
Lot.belongsTo(Round);




// const Lot = sequelize.define("Lot", {
//     lotId: {
//         type: Sequelize.INTEGER,
//         autoIncrement: true,
//         allowNull: false,
//         primaryKey: true,
//     },
//     lotName: {
//         type: Sequelize.STRING,
//         allowNull: false,
//     },
//     lotStateId: {
//         type: Sequelize.INTEGER,
//         allowNull: false,
//     },
//     lotRoundId: {
//         type: Sequelize.INTEGER,
//         allowNull: false,
//     },
// });

Lot.save = async(data) => {
    try {
        return await Lot.create(data);
    } catch (error) {
        console.log(error);
        return { "error": error }
    }
};

module.exports = Lot;