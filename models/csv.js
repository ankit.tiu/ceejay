const { Parser } = require('json2csv');
const qr = require('qrcode');
const Csv = {};

Csv.get_csv = (fields, data, title, res, name) => {
    try {
        const parser = new Parser({ fields, header: false });
        res.attachment('data.csv');
        res.status(200).send(title + parser.parse(data));
        //   res.sendFile("/data.csv");
    } catch (err) {
        console.error(err);
        res.send(err);
    }
}
Csv.get_qr = (fields, data, res, name) => {
    let strData = JSON.stringify(data)
    try {
        qr.toString(strData, function(err, code) {
            if (err) return res.send("error occurred")
            res.send(code)
        });
        qr.toDataURL(strData, function(err, code) {
            if (err) return res.send("error occurred")
            res.send(code)
        })
    } catch (err) {
        console.error(err);
        res.send(err);
    }
}
module.exports = Csv;