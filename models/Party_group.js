const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const Party_group = sequelize.define("party_group", {
    party_group_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    financialYearFinancialYearId : {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    stateStateId : {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    roundRoundId : {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    party_group_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
},{paranoid: true,});

module.exports = Party_group;