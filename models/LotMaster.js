const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const LotMaster = sequelize.define("LotMaster", {
    lotMasterId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    lotMasterMo: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
    },
    lotMasterName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterPlace: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterGodown: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterActb: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterStanb: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lotMasterDistu: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterRound: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterOption: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lotMasterUpset: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lotMasterLRate: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lotMasterWT: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    lotMasterExp: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    lotMasterRoundPass: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
});

module.exports = LotMaster;