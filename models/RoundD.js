const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");


const RoundD = sequelize.define("roundd", {
    roundd_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    roundd_year: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    roundd_state: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    round_round_id: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    roundd_date: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    roundd_set: {
        type: Sequelize.BOOLEAN,
        // allowNull: false,
    },
    // roundd_s_date: {
    //     type: Sequelize.STRING,
    //     allowNull: false,
    // },
    // roundd_t_date: {
    //     type: Sequelize.STRING,
    //     allowNull: false,
    // },
});
module.exports = RoundD;