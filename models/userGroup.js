const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const userGroup = sequelize.define("userGroup", {
    userId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    groupId: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});

module.exports = userGroup;