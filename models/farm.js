const Sequelize = require("sequelize");

const sequelize = require("../util/database");

const Farm = sequelize.define("farm", {
    farmId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    farmName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    farmAddress: {
        type: Sequelize.STRING,
        allowNull: false,
    }
},{paranoid: true});

module.exports = Farm;