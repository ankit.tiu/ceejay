const financial_year = require("./Financial_year");
const Groups = require("./groups");
const Lot = require("./lot");
const Party = require("./Party");
const Party_lot = require("./Party_lot");
const Round = require("./Round");
const State = require("./State");
const User = require("./users");
const Party_group = require("./Party_group");
const RoundD = require("./RoundD");

Party.hasMany(Party_lot);
// Party.hasOne(Party_lot);
Party.belongsToMany(Lot, { through: 'party_lots' });
// Party.belongsToMany(Party_lot);
Party.belongsTo(financial_year);
Party.belongsTo(State);
Party.belongsTo(Round);
// Party.belongsTo(Groups);


Lot.belongsToMany(Party, { through: 'party_lots' });
Lot.hasMany(Party_lot);

Party_lot.belongsTo(Party);
Party_lot.belongsTo(financial_year);
Party_lot.belongsTo(State);
Party_lot.belongsTo(Round);
Party_lot.belongsTo(Lot);

Party_group.belongsToMany(Party, { through: 'party_partyGroup' });


User.belongsTo(Groups);

// Party_group.hasMany(Party);