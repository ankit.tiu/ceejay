const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");
const Lot = require("./lot");
const Party_lot = require("./Party_lot");
const financial_year = require("./Financial_year");
const State = require("./State");
const Round = require("./Round");
// const Party_lot = require("./Party_lot");

const Party = sequelize.define("party", {
    party_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    financialYearFinancialYearId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    stateStateId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    roundRoundId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    party_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    party_display_name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    party_emd: {
        type: Sequelize.DECIMAL,
        allowNull: true,
    },
    party_purchase_capacity: {
        type: Sequelize.DECIMAL,
        allowNull: true,
    },
    party_bidding_capacity: {
        type: Sequelize.DECIMAL,
        allowNull: true,
    },
    party_head_code: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    party_status: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    party_balance: {
        type: Sequelize.DECIMAL,
        allowNull: true,
    },
}, { paranoid: true, });

Party.findById = (party_id, result) => {
    sql.query(`SELECT * FROM party WHERE party_id = ${party_id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            // console.log("found users: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Party.find = async(round_id, state_id, financial_year_id, element) => {
    // sql.query(`SELECT * FROM parties WHERE party_financial_year_id = ${financial_year_id} AND party_state_id = ${state_id} AND party_round_id = ${round_id} AND party_name = '${element}';`).then((x,d)=>{
    //     console.log(d);
    // });

    return await Party.findAll({
        where: {
            financialYearFinancialYearId: financial_year_id,
            stateStateId: state_id,
            roundRoundId: round_id,
            party_name: element
        }
    });

    // console.log($row);

    // sql.query(`SELECT * FROM parties WHERE party_financial_year_id = ${financial_year_id} AND party_state_id = ${state_id} AND party_round_id = ${round_id} AND party_name = '${element}';`, (err, res) => {
    //     if (err) {
    //         console.log("error: ", err);
    //         result(err, null);
    //         return;
    //     }
    //     if (res.length) {
    //         //console.log("found users: ", res[0]);
    //         result(null, res[0]);
    //         return;
    //     }
    //     // not found Customer with the id
    //     result({ kind: "not_found" }, null);
    // });
};

Party.capacity = async(search) => {
    return await Party.findAll({
        where: {
            party_financial_year_id: search.financial_year_id,
            party_state_id: search.round_id,
            party_round_id: search.state_id
        },
        include: [Lot]
    });
}

Party.set_round_party = async(params) => {
    if (params.party_list != undefined) {
        params.party_list.forEach(async(element, index) => {
            $row = await Party.find(params.master.round_id, params.master.state_id, params.master.financial_year_id, element);
            if ($row.length == 0)
                Party.create({
                    financialYearFinancialYearId: params.master.financial_year_id,
                    stateStateId: params.master.state_id,
                    roundRoundId: params.master.round_id,
                    party_name: element,
                    party_head_code: (index + 1)
                });
        });
        return true;
    } else return false;
}

Party.getAll = result => {
    sql.query("SELECT * FROM party", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        // console.log("user: ", res);
        result(null, res);
    });
};

Party.updateById = (id, customer, result) => {
    sql.query(
        "UPDATE party SET email = ?, name = ?, active = ? WHERE id = ?", [customer.email, customer.name, customer.active, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated customer: ", { id: id, ...customer });
            result(null, { id: id, ...customer });
        }
    );
};

Party.remove = (id, result) => {
    sql.query("DELETE FROM party WHERE party_id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
};

Party.removeAll = result => {
    sql.query("DELETE FROM party", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} customers`);
        result(null, res);
    });
};

module.exports = Party;