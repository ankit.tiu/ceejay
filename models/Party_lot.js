const Sequelize = require("sequelize");
const sequelize = require("../util/database");
const sql = require("./db.js");
const Lot = require("./lot");
// const Party = require("./Party");
const financial_year = require("./Financial_year");
const State = require("./State");
const Round = require("./Round");
const Party = require("./Party");

const Party_lot = sequelize.define("party_lot_data", {
    party_lot_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    party_lot_priority: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    // party_lot_party_id: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    // },
    // party_lot_capt_id: {
    //     type: Sequelize.INTEGER,
    //     allowNull: true,
    // },
    // party_lot_lot_id: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    // },
    party_lot_result_flag: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    party_lot_pur_rate: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    party_lot_qty_per_rate: {
        type: Sequelize.DECIMAL,
        allowNull: false,
    },
    party_lot_rank: {
        type: Sequelize.DECIMAL,
        allowNull: true,
    },
    party_lot_status: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    party_lot_result_status: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
    },
    partyPartyId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    LotLotId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    financialYearFinancialYearId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    stateStateId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    roundRoundId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
}, { paranoid: true });


// Party_lot.belongsTo(Lot);
// Party_lot.belongsTo(Party, {foreignKey: 'party_lot_party_id'});

Party_lot.save_array = async(search, party, party_lot) => {
    var rows = await Lot.findAll({
        where: {
            financialYearFinancialYearId: search.financial_year_id,
            // roundRoundId: search.round_id,
            stateStateId: search.state_id,
        }
    });
    // console.log(rows);

    var lots = [];
    party_lot.forEach(async(x, i) => {
        var row = rows.find(y => y.lotNo == x.lotNo);
        try {
            // var [result, metadata] = await sequelize.query("INSERT INTO `party_lots` (`createdAt`, `updatedAt`, `partyPartyId`, `LotLotId`) VALUES ('2021-10-22 16:12:35', '2021-10-22 16:12:35', '" + party.party_id + "', '" + row.lotId + "');");
            // Party.addParty_lots({"lotId" : row.lotId, "party_id" : party.party_id});
            x.LotLotId = row.lotId;
            x.roundRoundId = search.round_id;
            x.partyPartyId = party.party_id;
            x.party_lot_party_id = party.party_id;
            x.stateStateId = search.state_id;
            x.financialYearFinancialYearId = search.financial_year_id;
            lots.push(x);
            await Party_lot.create(x);
        } catch (error) {
            flag = false;
            console.log(error);
        }
    });
    // console.log(party.update(lots));
    // console.log(lots);
}

Party_lot.loadpartywise = async(search) => {
    // return await Party.findAll({
    //     where: {
    //         party_financial_year_id: search.financial_year_id,
    //         party_state_id: search.round_id,
    //         party_round_id: search.state_id
    //     }, 
    //     // include: Party_lot
    //   });
    // return await Party.capacity(search);
    // return "Hi";
    // return await Party.findAll({
    //     where: {
    //         party_financial_year_id: search.financial_year_id,
    //         party_state_id: search.round_id,
    //         party_round_id: search.state_id
    //     }, 
    //     // include: Party_lot
    //   });
}


module.exports = Party_lot;